#include "SuchenForm.h"
//#include "Globalevariablen.h"
#include "AnzeigenForm.h"
#include "Datentypen.h"

#include <string>
#include <string.h>
#include <msclr\marshal_cppstd.h>
#include <stdbool.h> 
#include <iostream>
#include <conio.h>

using msclr::interop::marshal_as;


//----Eventhandler f�r Suchen-Button
System::Void INF3_Projekt::SuchenForm::button4_Click(System::Object^  sender, System::EventArgs^  e){
	extern STUDENT Eintrag[150];
	extern int anzahl; //gibt die Anzahl der Eintr�ge im Array wieder
	extern int nummer;
	extern int ha[150]; // Hilfsarray in dem die Ergenisse gespeicher werden
	int l; // Laufvariable
	extern int Ergebnisanzahl; // Variable in der die Anzahl der Ergebnisse eingespeichert wird
	extern int bearbeiten;


	using std::string;

	extern int zaehlen(); // z�hlen der Eintr�ge im Array


	if (String::IsNullOrEmpty(textBox1->Text))
	{															//�berpr�fen, ob der String leer ist
		MessageBox::Show("Es wurde kein Suchbegriff eingegeben. Bitte geben Sie ein Suchbegriff ein.",
			"Suchen", MessageBoxButtons::OK,
			MessageBoxIcon::Asterisk);
	}
	else
	//Wenn String (Also TextBox-Eingabe) nicht leer, wird die Suchkategorie abgefrage und der entsprechende Suchalgorithmus durchgef�hrt
	
	{
		if (comboBox1->SelectedItem == "Matrikelnummer")		//Suche nach Matrikelnummer
		{

				int Eingabemat = 0;		// Variable, in der der eingetragene Wert "abgespeichert" wird
			int i = 0;					//laufvariable f�r schleife
			char test[50];				//Array, in das der Suchbegriff geschrieben wird
			bool istnichtzahl = false;	//Pr�fvariable zum Test, ob Eingabe eine Zahl ist

			sprintf(test, "%s", textBox1->Text);	//schreiben der Eingabe in test
			int laenge = strlen(test);				//Eingabel�nge pr�fen

			


			if (laenge <= 6)						//Zahlen �ber 999999 werden nicht akzeptiert, daher max. 6-mal durchlaufen der Testschleife
			{
				while (i <= laenge - 1)				//Pr�fen, ob alle Zeichen im eingegebenen Array Ziffern sind
				{
					if (isdigit(test[i]))			//Pr�ffunktion
					{
					}
					else
					{
						istnichtzahl = true;		//Wenn ein Zeichen nicht Zahl, dann Pr�fvariable = true
					}
					i++;
				}
			}
			else
			{
				istnichtzahl = true;				//Zahlen �ber 999999 werden nicht als Zahl gewertet

			}



			if (istnichtzahl == false)				//Ergebnis abfragen, ob alle Zeichen der Eingabe Ziffern sind. 
			{
				Eingabemat = atoi(test);			//Umwandeln der Eingabe in Integer

				if (Eingabemat >= 0 && Eingabemat < 999999)
				{ //Bedingung zum Abfangen falscher Eingaben. Beim Eingeben von Matrikelnummern >999999 wird zur�ck zur Auswahl des Suchmerkmals gesprungen

					Eingabemat = Convert::ToInt32(textBox1->Text);	

					Ergebnisanzahl = 0;				

					// durchsuchen des ADT nach �bereinstimmungen mit der Eingabemat
					for (l = 0; l < anzahl; l++)
					{
						if (Eingabemat == Eintrag[l].Studentname.Matrikelnummer)
						{
							ha[Ergebnisanzahl] = l; // Abspeichern des gefundenen Indexes in einem Hilfsarray; Ergebnisanzahl wurde zu Beginn mit Ergebnisanzahl = 0 definiert damit der erste gefundene Index in ha[0] gespeichert wird
							Ergebnisanzahl++;		// es wird ein Suchtreffer hinzugez�hlt
						}
					}

					if (Ergebnisanzahl >= 1) {		// ist min. ein Suchtreffer vorhanden, dann werden die Ergebnise ausgegeben

						
						nummer = ha[0];
						AnzeigenForm^ANZ = gcnew AnzeigenForm(4, nummer);   // Objekt von AnzeigenForm erstellen mit bearbtiten = false (--> Textboxen disabled)
						ANZ->ShowDialog();
					}

					if (Ergebnisanzahl == 0)
					{														//Wenn alle Datens�tze durchsucht wurden und die Variable der Suchergebnisse immernoch bei 0 steht
						MessageBox::Show("Es wurde kein Datensatz gefunden.",												//Ausgabe: Kein Datensatz gefunden
							"Suche fehlgeschlagen", MessageBoxButtons::OK,
							MessageBoxIcon::Asterisk);
					}
				}
			}
			else
			{
				MessageBox::Show("Falsche Eingabe! Bitte geben Sie eine Nummer zwischen 0 und 999999 ein.", "Falsche Eingabe!",
					MessageBoxButtons::OK,
					MessageBoxIcon::Asterisk);
			}

		}
		else if (comboBox1->SelectedItem == "Vorname")			//Suche nach Vornamen
		{

			char Eingabevorname[50]; // Char, in dem der eingetragene Wert "abgespeichert" wird
			string Eingabevor1 = marshal_as<string>(textBox1->Text);
			strcpy(Eingabevorname, Eingabevor1.c_str());
			Ergebnisanzahl = 0;

			// durchsuchen des ADT nach �bereinstimmungen mit dem Eingabevorname
			for (l = 0; l < anzahl; l++)
			{
				if (strcmp(Eingabevorname, Eintrag[l].Studentname.Vorname) == 0)
				{
					ha[Ergebnisanzahl] = l;		// Abspeichern des gefundenen Indexes in einem Hilfsarray; Ergebnisanzahl wurde zu Beginn mit Ergebnisanzahl = 0 definiert damit der erste gefundene Index in ha[0] gespeichert wird
					Ergebnisanzahl++;			// es wird ein Suchtreffer hinzugez�hlt
				}
			}

			if (Ergebnisanzahl >= 1) {			// ist min. ein Suchtreffer vorhanden, dann werden die Ergebnise ausgegeben

				nummer = ha[0];
				AnzeigenForm^ANZ = gcnew AnzeigenForm(4, nummer);				// Objekt von AnzeigenForm erstellen mit bearbtiten = false (--> Textboxen disabled)
				ANZ->ShowDialog();
			}

			if (Ergebnisanzahl == 0){											//Wenn alle Datens�tze durchsucht wurden und die Variable der Suchergebnisse immernoch bei 0 steht
				MessageBox::Show("Es wurde kein Datensatz gefunden.",			//Ausgabe: Kein Datensatz gefunden
					"Suche fehlgeschlagen", MessageBoxButtons::OK,
					MessageBoxIcon::Asterisk);
			}

		}
		else if (comboBox1->SelectedItem == "Name")				//Suche nach Nachnamen
		{

				char Eingabenachname[50]; // Char, in dem der eingetragene Wert "abgespeichert" wird
			string Eingabenach1 = marshal_as<string>(textBox1->Text);
			strcpy(Eingabenachname, Eingabenach1.c_str());
			Ergebnisanzahl = 0;

			// durchsuchen des ADT nach �bereinstimmungen mit der Eingabenachnamen
			for (l = 0; l < anzahl; l++)
			{
				if (strcmp(Eingabenachname, Eintrag[l].Studentname.Name) == 0)
				{
					ha[Ergebnisanzahl] = l; // Abspeichern des gefundenen Indexes in einem Hilfsarray; Ergebnisanzahl wurde zu Beginn mit Ergebnisanzahl = 0 definiert damit der erste gefundene Index in ha[0] gespeichert wird
					Ergebnisanzahl++; // es wird ein Suchtreffer hinzugez�hlt
				}
			}

			if (Ergebnisanzahl >= 1) { // ist min. ein Suchtreffer vorhanden, dann werden die Ergebnise ausgegeben

				nummer = ha[0];
				AnzeigenForm^ANZ = gcnew AnzeigenForm(4, nummer);   // Objekt von AnzeigenForm erstellen mit bearbtiten = false (--> Textboxen disabled)
				ANZ->ShowDialog();
			}

			if (Ergebnisanzahl == 0){																	//Wenn alle Datens�tze durchsucht wurden und die Variable der Suchergebnisse immernoch bei 0 steht
				MessageBox::Show("Es wurde kein Datensatz gefunden.",												//Ausgabe: Kein Datensatz gefunden
					"Suche fehlgeschlagen", MessageBoxButtons::OK,
					MessageBoxIcon::Asterisk);
			}

		}
		else if (comboBox1->SelectedItem == "Praxispartner")	//Suche nach Praxispartner
		{

			char Eingabepraxis[50]; // Char, in dem der eingetragene Wert "abgespeichert" wird
			string Eingabepraxis1 = marshal_as<string>(textBox1->Text);
			strcpy(Eingabepraxis, Eingabepraxis1.c_str());
			Ergebnisanzahl = 0;

			// durchsuchen des ADT nach �bereinstimmungen mit der Eingabepraxis
			for (l = 0; l < anzahl; l++)
			{
				if (strcmp(Eingabepraxis, Eintrag[l].Firmaname) == 0)
				{
					ha[Ergebnisanzahl] = l; // Abspeichern des gefundenen Indexes in einem Hilfsarray; Ergebnisanzahl wurde zu Beginn mit Ergebnisanzahl = 0 definiert damit der erste gefundene Index in ha[0] gespeichert wird
					Ergebnisanzahl++; // es wird ein Suchtreffer hinzugez�hlt
				}
			}

			if (Ergebnisanzahl >= 1) { // ist min. ein Suchtreffer vorhanden, dann werden die Ergebnise ausgegeben

				nummer = ha[0];
				AnzeigenForm^ANZ = gcnew AnzeigenForm(4, nummer);   // Objekt von AnzeigenForm erstellen mit bearbtiten = false (--> Textboxen disabled)
				ANZ->ShowDialog();
			}

			if (Ergebnisanzahl == 0){																	//Wenn alle Datens�tze durchsucht wurden und die Variable der Suchergebnisse immernoch bei 0 steht
				MessageBox::Show("Es wurde kein Datensatz gefunden.",												//Ausgabe: Kein Datensatz gefunden
					"Suche fehlgeschlagen", MessageBoxButtons::OK,
					MessageBoxIcon::Asterisk);
			}

		}
		else if (comboBox1->SelectedItem == "Studienbeginn")	//Suche nach Studienbeginn
		{

			int Eingabejahr = 0;	// Variable, in der der eingetragene Wert "abgespeichert" wird
			int i = 0;				//laufvariable f�r schleife
			char test[50];
			bool istnichtzahl = false;

			sprintf(test, "%s", textBox1->Text);
			int laenge = strlen(test);


			while (i <= laenge - 1)				//Pr�fen, ob alle Zeichen im eingegebenen Array Ziffern sind
			{
				if (isdigit(test[i]))
				{
				}
				else
				{
					istnichtzahl = true;		//Wenn nicht, dann true
				}
				i++;
			}


			if (istnichtzahl == false)			//Ergebnis abfragen, ob alle Zeichen der Eingabe Ziffern sind. 
			{
				Eingabejahr = atoi(test);		//Umwandeln der Eingabe in Integer

				if (Eingabejahr >= 2000 && Eingabejahr < 9999)
				{ //Bedingung zum Abfangen falscher Eingaben

					Eingabejahr = Convert::ToInt32(textBox1->Text);

					Ergebnisanzahl = 0;

					// durchsuchen des ADT nach �bereinstimmungen mit der Eingabemat
					for (l = 0; l < anzahl; l++)
					{
						if (Eingabejahr == Eintrag[l].Studentname.Jahr)
						{
							ha[Ergebnisanzahl] = l; // Abspeichern des gefundenen Indexes in einem Hilfsarray; Ergebnisanzahl wurde zu Beginn mit Ergebnisanzahl = 0 definiert damit der erste gefundene Index in ha[0] gespeichert wird
							Ergebnisanzahl++;		// es wird ein Suchtreffer hinzugez�hlt
						}
					}

					if (Ergebnisanzahl >= 1) { // ist min. ein Suchtreffer vorhanden, dann werden die Ergebnise ausgegeben

						//SuchtrefferForm1^SUCHTR = gcnew SuchtrefferForm1();
						//SUCHTR->ShowDialog();
						nummer = ha[0];
						AnzeigenForm^ANZ = gcnew AnzeigenForm(4, nummer);   // Objekt von AnzeigenForm erstellen mit bearbteiten = false (--> Textboxen disabled)
						ANZ->ShowDialog();
					}

					if (Ergebnisanzahl == 0)
					{														//Wenn alle Datens�tze durchsucht wurden und die Variable der Suchergebnisse immernoch bei 0 steht
						MessageBox::Show("Es wurde kein Datensatz gefunden.",												//Ausgabe: Kein Datensatz gefunden
							"Suche fehlgeschlagen", MessageBoxButtons::OK,
							MessageBoxIcon::Asterisk);
					}
				}
			}
			else
			{
				MessageBox::Show("Falsche Eingabe! Bitte geben Sie eine Nummer zwischen 2000 und 9999 ein.", "Falsche Eingabe!",
					MessageBoxButtons::OK,
					MessageBoxIcon::Asterisk);
			}

		}
		else
		//Wenn keine Suchkategorie ausgew�hlt
		{
			MessageBox::Show("Bitte eine Suchkategorie ausw�hlen!", "Kategorie w�hlen",
				MessageBoxButtons::OK,
				MessageBoxIcon::Asterisk);
		}
	}
};
