#pragma once
#include <stdio.h>
#include<iostream>
#include<stdlib.h>
#include<fstream>
#include <Windows.h>
#include <shellapi.h>
#include <string>


#include"Globalevariablen.h";
#include "AnzeigenForm.h";
#include "EingebenForm.h";
#include "SuchenForm.h";
# include "HilfeHauptmForm.h";








namespace INF3_Projekt {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Zusammenfassung f�r HauptForm
	/// </summary>
	public ref class HauptForm : public System::Windows::Forms::Form
	{
	public:
		HauptForm()
		{
			
			InitializeComponent();
			//
			//TODO: Konstruktorcode hier hinzuf�gen.


			// Anzeige, dass Datens�tze geladen wurden und wie viele von den max. 150 belegt sind
			einlesen();
			if (einlesen()==2)
			{
				label3->Text = Convert::ToString("Datens�tze geladen.");
			}
			else label3->Text = Convert::ToString("keine Datens�tze geladen!");
			
		}

	protected:
		/// <summary>
		/// Verwendete Ressourcen bereinigen.
		/// </summary>
		~HauptForm()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Button^  button1;
	private: System::Windows::Forms::Button^  button2;
	private: System::Windows::Forms::Button^  button3;


	private: System::Windows::Forms::Button^  button6;
	private: System::Windows::Forms::Button^  button7;
	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::Label^  label3;
	private: System::Windows::Forms::Button^  button4;


	protected:

	private:
		/// <summary>
		/// Erforderliche Designervariable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Erforderliche Methode f�r die Designerunterst�tzung.
		/// Der Inhalt der Methode darf nicht mit dem Code-Editor ge�ndert werden.
		/// </summary>
		void InitializeComponent()
		{
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->button2 = (gcnew System::Windows::Forms::Button());
			this->button3 = (gcnew System::Windows::Forms::Button());
			this->button6 = (gcnew System::Windows::Forms::Button());
			this->button7 = (gcnew System::Windows::Forms::Button());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->button4 = (gcnew System::Windows::Forms::Button());
			this->SuspendLayout();
			// 
			// button1
			// 
			this->button1->Location = System::Drawing::Point(190, 128);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(81, 45);
			this->button1->TabIndex = 0;
			this->button1->Text = L"Datens�tze anzeigen";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &HauptForm::button1_Click);
			// 
			// button2
			// 
			this->button2->Location = System::Drawing::Point(103, 128);
			this->button2->Name = L"button2";
			this->button2->Size = System::Drawing::Size(81, 45);
			this->button2->TabIndex = 1;
			this->button2->Text = L"Datensatz suchen";
			this->button2->UseVisualStyleBackColor = true;
			this->button2->Click += gcnew System::EventHandler(this, &HauptForm::button2_Click);
			// 
			// button3
			// 
			this->button3->Cursor = System::Windows::Forms::Cursors::Arrow;
			this->button3->Location = System::Drawing::Point(16, 128);
			this->button3->Name = L"button3";
			this->button3->Size = System::Drawing::Size(81, 45);
			this->button3->TabIndex = 2;
			this->button3->Text = L"Datensatz eingeben";
			this->button3->UseVisualStyleBackColor = true;
			this->button3->Click += gcnew System::EventHandler(this, &HauptForm::button3_Click);
			// 
			// button6
			// 
			this->button6->Location = System::Drawing::Point(415, 328);
			this->button6->Name = L"button6";
			this->button6->Size = System::Drawing::Size(109, 24);
			this->button6->TabIndex = 5;
			this->button6->Text = L"Daten speichern";
			this->button6->UseVisualStyleBackColor = true;
			this->button6->Click += gcnew System::EventHandler(this, &HauptForm::button6_Click);
			// 
			// button7
			// 
			this->button7->Location = System::Drawing::Point(530, 328);
			this->button7->Name = L"button7";
			this->button7->Size = System::Drawing::Size(109, 24);
			this->button7->TabIndex = 6;
			this->button7->Text = L"Beenden";
			this->button7->UseVisualStyleBackColor = true;
			this->button7->Click += gcnew System::EventHandler(this, &HauptForm::button7_Click);
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label1->Location = System::Drawing::Point(12, 9);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(178, 20);
			this->label1->TabIndex = 7;
			this->label1->Text = L"Studentendatenbank";
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label2->Location = System::Drawing::Point(13, 44);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(346, 16);
			this->label2->TabIndex = 8;
			this->label2->Text = L"Willkommen! Bitte w�hlen Sie die gew�nschte Aktion aus.";
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(16, 84);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(35, 13);
			this->label3->TabIndex = 9;
			this->label3->Text = L"label3";
			// 
			// button4
			// 
			this->button4->Location = System::Drawing::Point(16, 328);
			this->button4->Name = L"button4";
			this->button4->Size = System::Drawing::Size(75, 23);
			this->button4->TabIndex = 10;
			this->button4->Text = L"Hilfe";
			this->button4->UseVisualStyleBackColor = true;
			this->button4->Click += gcnew System::EventHandler(this, &HauptForm::button4_Click);
			// 
			// HauptForm
			// 
			this->AccessibleDescription = L"";
			this->AccessibleName = L"";
			this->AutoSize = true;
			this->ClientSize = System::Drawing::Size(651, 364);
			this->Controls->Add(this->button4);
			this->Controls->Add(this->label3);
			this->Controls->Add(this->label2);
			this->Controls->Add(this->label1);
			this->Controls->Add(this->button7);
			this->Controls->Add(this->button6);
			this->Controls->Add(this->button3);
			this->Controls->Add(this->button2);
			this->Controls->Add(this->button1);
			this->MaximumSize = System::Drawing::Size(667, 402);
			this->MinimumSize = System::Drawing::Size(667, 402);
			this->Name = L"HauptForm";
			this->Text = L"Studentendatenbank";
			this->Load += gcnew System::EventHandler(this, &HauptForm::HauptForm_Load);
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e) //Anzeigen-Button
	{
		extern int nummer;
		extern bool meldung;
		meldung = true; // Hinweisbox beim Dr�cken von "Schlie�en" ist ausgeschaltet. Will man das Anzeigefenster schlie�en, kommt kein Warnhinweis.

		nummer = 0;										//beim Anzeigen immer bei null anfangen
		AnzeigenForm^ANZ = gcnew AnzeigenForm(2, 0);	//Objekt von AnzeigenForm erstellen mit bearbtiten = false (--> Textboxen disabled)
		ANZ->ShowDialog();
	}

private: System::Void button7_Click(System::Object^  sender, System::EventArgs^  e) //Beenden-Button
	{
		
		if (MessageBox::Show(							//Sicherheitsabfrage zum Beenden
			"Soll die Studentendatenbank wirklich beendet werden? Nicht gespeicherte Eingaben gehen dabei verloren.",
			"Studentendatenbank beenden?", MessageBoxButtons::YesNo,
			MessageBoxIcon::Question) == ::System::Windows::Forms::DialogResult::Yes)
		{

			Close();									//Schlie�en, falls Antwort == "Ja"
		}
	}		// Beenden-Button

private: System::Void button3_Click(System::Object^  sender, System::EventArgs^  e)	//Eingeben-Button
	{
	
		extern int zaehlen();
		extern bool meldung;
		meldung = false; // Hinweisbox beim Dr�cken von "Schlie�en" ist scharf geschaltet. Sind Felder ausgef�llt und man will schlie�en, kommt ein Warnhinweis.

		nummer = anzahl;
			
		if (anzahl <= 149)
		{
			AnzeigenForm^EING = gcnew AnzeigenForm(1, anzahl);   // Objekt von AnzeigenForm erstellen mit bearbtiten = true (--> Textboxen enabled)
			EING->ShowDialog(this);
		}
		else
		{
			MessageBox::Show("Es wurden bereits 150 Datens�tze eingegeben. Bitte l�schen Sie zun�chst Eintr�ge, bevor Sie neue eingeben.",
			"Datenbank voll", MessageBoxButtons::OK,
			MessageBoxIcon::Asterisk);
		}
	}		//Eingeben-Button
	
private: System::Void button2_Click(System::Object^  sender, System::EventArgs^  e) //Suchen-Button
	{

		SuchenForm^SUCH = gcnew SuchenForm();		// Objekt von SuchenForm erstellen:
		SUCH->ShowDialog();
	}		//Suchen-Button

public: System::Void button6_Click(System::Object^  sender, System::EventArgs^  e)  //SPEICHERN
	{
		extern int nummer;
		extern STUDENT Eintrag[150];
		extern int anzahl;
	
		zaehlen();
		FILE* fp = fopen("datenbank.dat", "wb");						//bin�res �ffnen der Datei zum ablegen der Daten
		fwrite(Eintrag, sizeof(STUDENT), anzahl, fp);					// fwrite (Quelle, gr��e, x-mal(anzahl), filepointer)
		fclose(fp);

		//int MessageBox;
		
		MessageBox::Show("Speichern der Datenbank erfolgreich!",
		"Speichern", MessageBoxButtons::OK,
		MessageBoxIcon::Asterisk);
	}		//Speichern-Button

private: System::Void button4_Click_1(System::Object^  sender, System::EventArgs^  e) 
	{
	}
private: System::Void HauptForm_Load(System::Object^  sender, System::EventArgs^  e) 
	{
	}



private: System::Void button4_Click(System::Object^  sender, System::EventArgs^  e) 
{
	HilfeHauptmForm^Hilfe2 = gcnew HilfeHauptmForm();
	Hilfe2->ShowDialog();
}
};
}
