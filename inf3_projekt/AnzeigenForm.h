#pragma once

#include <string>
#include "HilfeForm.h"


namespace INF3_Projekt {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	


	
	/// <summary>
	/// Zusammenfassung f�r AnzeigenForm
	/// </summary>
	public ref class AnzeigenForm : public System::Windows::Forms::Form
	{
		
	public:
		int bearbeitenhilf;
		AnzeigenForm(int bearbeiten, extern int nummer) //�bergebene Variable "bearbeiten" f�r verschiedene Datstellungen des "Anzeigen"-Fensters; nummer ist Laufvariable f�r Datenbank
		{
			bearbeitenhilf = bearbeiten;				//"bearbeitenhilf" ist eine hilfsvariable, um "bearbeiten" besser verarbeiten zu k�nnen
			
			InitializeComponent();						//bearbeiten
			//
			//TODO: Konstruktorcode hier hinzuf�gen.
			bearbeitenumschaltenumschalten(bearbeiten);	//Funktion zur Darstellung/Umschaltung der Verschiedenen Layouts von "Anzeigen", siehe unten
			//
		}

		

	protected:
		/// <summary>
		/// Verwendete Ressourcen bereinigen.
		/// </summary>
		~AnzeigenForm()
		{
			if (components)
			{
				delete components;
			}
		}


	private: System::Windows::Forms::Button^  button1;
	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::Label^  label3;
	private: System::Windows::Forms::Label^  label4;
	private: System::Windows::Forms::Label^  label5;
	private: System::Windows::Forms::Label^  label6;
	private: System::Windows::Forms::Label^  label7;
	private: System::Windows::Forms::Label^  label8;
	private: System::Windows::Forms::Label^  label9;
	private: System::Windows::Forms::Label^  label10;
	private: System::Windows::Forms::Label^  label11;
	private: System::Windows::Forms::Label^  label12;
	private: System::Windows::Forms::Label^  label13;
	private: System::Windows::Forms::Label^  label14;
	private: System::Windows::Forms::Label^  label15;
	private: System::Windows::Forms::Label^  label16;
	private: System::Windows::Forms::Label^  label17;
	private: System::Windows::Forms::Label^  label18;
	private: System::Windows::Forms::Label^  label19;
	private: System::Windows::Forms::Label^  label20;
	private: System::Windows::Forms::Label^  label21;
	private: System::Windows::Forms::Label^  label22;
	private: System::Windows::Forms::TextBox^  textBox1; 
	private: System::Windows::Forms::TextBox^  textBox2;
	private: System::Windows::Forms::TextBox^  textBox3;
	private: System::Windows::Forms::TextBox^  textBox4;
	private: System::Windows::Forms::TextBox^  textBox5;
	private: System::Windows::Forms::TextBox^  textBox6;
	private: System::Windows::Forms::Button^  button2;
	private: System::Windows::Forms::Button^  button3;
	private: System::Windows::Forms::Button^  button4;
	private: System::Windows::Forms::TextBox^  textBox7;
	private: System::Windows::Forms::TextBox^  textBox8;
	private: System::Windows::Forms::TextBox^  textBox9;
	private: System::Windows::Forms::TextBox^  textBox10;
	private: System::Windows::Forms::TextBox^  textBox11;
	private: System::Windows::Forms::TextBox^  textBox12;
	private: System::Windows::Forms::TextBox^  textBox13;
	private: System::Windows::Forms::TextBox^  textBox14;
	private: System::Windows::Forms::TextBox^  textBox15;
	private: System::Windows::Forms::TextBox^  textBox16;
	private: System::Windows::Forms::TextBox^  textBox17;
	private: System::Windows::Forms::TextBox^  textBox18;
	private: System::Windows::Forms::TextBox^  textBox19;
	private: System::Windows::Forms::TextBox^  textBox20;
	private: System::Windows::Forms::TextBox^  textBox21;
	private: System::Windows::Forms::TextBox^  textBox22;
	private: System::Windows::Forms::Button^  button5;
	public: System::Windows::Forms::Button^  button6;
	private: System::Windows::Forms::Label^  label23;
	private: System::Windows::Forms::TextBox^  textBox23;
	private: System::Windows::Forms::Label^  label24;
	private: System::Windows::Forms::Label^  label25;
	private: System::Windows::Forms::Label^  label26;
	private: System::Windows::Forms::Label^  label27;
	private: System::Windows::Forms::Button^  button7;

	private:

	protected:

	private:
		/// <summary>
		/// Erforderliche Designervariable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Erforderliche Methode f�r die Designerunterst�tzung.
		/// Der Inhalt der Methode darf nicht mit dem Code-Editor ge�ndert werden.
		/// </summary>

		void InitializeComponent()
		{
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->label5 = (gcnew System::Windows::Forms::Label());
			this->label6 = (gcnew System::Windows::Forms::Label());
			this->label7 = (gcnew System::Windows::Forms::Label());
			this->label8 = (gcnew System::Windows::Forms::Label());
			this->label9 = (gcnew System::Windows::Forms::Label());
			this->label10 = (gcnew System::Windows::Forms::Label());
			this->label11 = (gcnew System::Windows::Forms::Label());
			this->label12 = (gcnew System::Windows::Forms::Label());
			this->label13 = (gcnew System::Windows::Forms::Label());
			this->label14 = (gcnew System::Windows::Forms::Label());
			this->label15 = (gcnew System::Windows::Forms::Label());
			this->label16 = (gcnew System::Windows::Forms::Label());
			this->label17 = (gcnew System::Windows::Forms::Label());
			this->label18 = (gcnew System::Windows::Forms::Label());
			this->label19 = (gcnew System::Windows::Forms::Label());
			this->label20 = (gcnew System::Windows::Forms::Label());
			this->label21 = (gcnew System::Windows::Forms::Label());
			this->label22 = (gcnew System::Windows::Forms::Label());
			this->textBox1 = (gcnew System::Windows::Forms::TextBox());
			this->textBox2 = (gcnew System::Windows::Forms::TextBox());
			this->textBox3 = (gcnew System::Windows::Forms::TextBox());
			this->textBox4 = (gcnew System::Windows::Forms::TextBox());
			this->textBox5 = (gcnew System::Windows::Forms::TextBox());
			this->textBox6 = (gcnew System::Windows::Forms::TextBox());
			this->button2 = (gcnew System::Windows::Forms::Button());
			this->button3 = (gcnew System::Windows::Forms::Button());
			this->button4 = (gcnew System::Windows::Forms::Button());
			this->textBox7 = (gcnew System::Windows::Forms::TextBox());
			this->textBox8 = (gcnew System::Windows::Forms::TextBox());
			this->textBox9 = (gcnew System::Windows::Forms::TextBox());
			this->textBox10 = (gcnew System::Windows::Forms::TextBox());
			this->textBox11 = (gcnew System::Windows::Forms::TextBox());
			this->textBox12 = (gcnew System::Windows::Forms::TextBox());
			this->textBox13 = (gcnew System::Windows::Forms::TextBox());
			this->textBox14 = (gcnew System::Windows::Forms::TextBox());
			this->textBox15 = (gcnew System::Windows::Forms::TextBox());
			this->textBox16 = (gcnew System::Windows::Forms::TextBox());
			this->textBox17 = (gcnew System::Windows::Forms::TextBox());
			this->textBox18 = (gcnew System::Windows::Forms::TextBox());
			this->textBox19 = (gcnew System::Windows::Forms::TextBox());
			this->textBox20 = (gcnew System::Windows::Forms::TextBox());
			this->textBox21 = (gcnew System::Windows::Forms::TextBox());
			this->textBox22 = (gcnew System::Windows::Forms::TextBox());
			this->button5 = (gcnew System::Windows::Forms::Button());
			this->button6 = (gcnew System::Windows::Forms::Button());
			this->label23 = (gcnew System::Windows::Forms::Label());
			this->textBox23 = (gcnew System::Windows::Forms::TextBox());
			this->label24 = (gcnew System::Windows::Forms::Label());
			this->label25 = (gcnew System::Windows::Forms::Label());
			this->label26 = (gcnew System::Windows::Forms::Label());
			this->label27 = (gcnew System::Windows::Forms::Label());
			this->button7 = (gcnew System::Windows::Forms::Button());
			this->SuspendLayout();
			// 
			// button1
			// 
			this->button1->Location = System::Drawing::Point(632, 426);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(90, 24);
			this->button1->TabIndex = 0;
			if (bearbeitenhilf == 1)
			{
				this->button1->Text = L"Schlie�en";
			}
			else if (bearbeitenhilf == 2)
			{
				this->button1->Text = L"Schlie�en";
			}
			else if (bearbeitenhilf == 3)
			{
				this->button1->Text = L"Schlie�en";
			}
			else if (bearbeitenhilf == 4)
			{
				this->button1->Text = L"Zum Suchmen�";
			}
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &AnzeigenForm::button1_Click_1);
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(82, 48);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(39, 13);
			this->label1->TabIndex = 1;
			this->label1->Text = L"Name*";
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(68, 79);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(53, 13);
			this->label2->TabIndex = 2;
			this->label2->Text = L"Vorname*";
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(36, 110);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(85, 13);
			this->label3->TabIndex = 3;
			this->label3->Text = L"Matrikelnummer*";
			// 
			// label4
			// 
			this->label4->AutoSize = true;
			this->label4->Location = System::Drawing::Point(13, 141);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(108, 13);
			this->label4->TabIndex = 4;
			this->label4->Text = L"Jahr (Studienbeginn)*";
			// 
			// label5
			// 
			this->label5->AutoSize = true;
			this->label5->Location = System::Drawing::Point(33, 172);
			this->label5->Name = L"label5";
			this->label5->Size = System::Drawing::Size(84, 13);
			this->label5->TabIndex = 5;
			this->label5->Text = L"Festnetznummer";
			// 
			// label6
			// 
			this->label6->AutoSize = true;
			this->label6->Location = System::Drawing::Point(48, 203);
			this->label6->Name = L"label6";
			this->label6->Size = System::Drawing::Size(69, 13);
			this->label6->TabIndex = 6;
			this->label6->Text = L"Mobilnummer";
			// 
			// label7
			// 
			this->label7->AutoSize = true;
			this->label7->Location = System::Drawing::Point(40, 234);
			this->label7->Name = L"label7";
			this->label7->Size = System::Drawing::Size(77, 13);
			this->label7->TabIndex = 7;
			this->label7->Text = L"E-Mail Adresse";
			// 
			// label8
			// 
			this->label8->AutoSize = true;
			this->label8->Location = System::Drawing::Point(79, 265);
			this->label8->Name = L"label8";
			this->label8->Size = System::Drawing::Size(38, 13);
			this->label8->TabIndex = 8;
			this->label8->Text = L"Stra�e";
			// 
			// label9
			// 
			this->label9->AutoSize = true;
			this->label9->Location = System::Drawing::Point(48, 296);
			this->label9->Name = L"label9";
			this->label9->Size = System::Drawing::Size(69, 13);
			this->label9->TabIndex = 9;
			this->label9->Text = L"Hausnummer";
			// 
			// label10
			// 
			this->label10->AutoSize = true;
			this->label10->Location = System::Drawing::Point(57, 327);
			this->label10->Name = L"label10";
			this->label10->Size = System::Drawing::Size(60, 13);
			this->label10->TabIndex = 10;
			this->label10->Text = L"Postleitzahl";
			// 
			// label11
			// 
			this->label11->AutoSize = true;
			this->label11->Location = System::Drawing::Point(79, 358);
			this->label11->Name = L"label11";
			this->label11->Size = System::Drawing::Size(32, 13);
			this->label11->TabIndex = 11;
			this->label11->Text = L"Stadt";
			// 
			// label12
			// 
			this->label12->AutoSize = true;
			this->label12->Location = System::Drawing::Point(409, 46);
			this->label12->Name = L"label12";
			this->label12->Size = System::Drawing::Size(72, 13);
			this->label12->TabIndex = 12;
			this->label12->Text = L"Praxispartner*";
			// 
			// label13
			// 
			this->label13->AutoSize = true;
			this->label13->Location = System::Drawing::Point(405, 79);
			this->label13->Name = L"label13";
			this->label13->Size = System::Drawing::Size(72, 13);
			this->label13->TabIndex = 13;
			this->label13->Text = L"Stra�e (Firma)";
			// 
			// label14
			// 
			this->label14->AutoSize = true;
			this->label14->Location = System::Drawing::Point(376, 110);
			this->label14->Name = L"label14";
			this->label14->Size = System::Drawing::Size(103, 13);
			this->label14->TabIndex = 14;
			this->label14->Text = L"Hausnummer (Firma)";
			// 
			// label15
			// 
			this->label15->AutoSize = true;
			this->label15->Location = System::Drawing::Point(383, 141);
			this->label15->Name = L"label15";
			this->label15->Size = System::Drawing::Size(94, 13);
			this->label15->TabIndex = 15;
			this->label15->Text = L"Postleitzahl (Firma)";
			// 
			// label16
			// 
			this->label16->AutoSize = true;
			this->label16->Location = System::Drawing::Point(413, 172);
			this->label16->Name = L"label16";
			this->label16->Size = System::Drawing::Size(66, 13);
			this->label16->TabIndex = 16;
			this->label16->Text = L"Stadt (Firma)";
			// 
			// label17
			// 
			this->label17->AutoSize = true;
			this->label17->Location = System::Drawing::Point(394, 195);
			this->label17->Name = L"label17";
			this->label17->Size = System::Drawing::Size(85, 26);
			this->label17->TabIndex = 17;
			this->label17->Text = L"Ansprechpartner\n                Name";
			// 
			// label18
			// 
			this->label18->AutoSize = true;
			this->label18->Location = System::Drawing::Point(394, 226);
			this->label18->Name = L"label18";
			this->label18->Size = System::Drawing::Size(85, 26);
			this->label18->TabIndex = 18;
			this->label18->Text = L"Ansprechpartner\n            Vorname";
			// 
			// label19
			// 
			this->label19->AutoSize = true;
			this->label19->Location = System::Drawing::Point(433, 265);
			this->label19->Name = L"label19";
			this->label19->Size = System::Drawing::Size(44, 13);
			this->label19->TabIndex = 19;
			this->label19->Text = L"Position";
			// 
			// label20
			// 
			this->label20->AutoSize = true;
			this->label20->Location = System::Drawing::Point(393, 296);
			this->label20->Name = L"label20";
			this->label20->Size = System::Drawing::Size(84, 13);
			this->label20->TabIndex = 20;
			this->label20->Text = L"Festnetznummer";
			// 
			// label21
			// 
			this->label21->AutoSize = true;
			this->label21->Location = System::Drawing::Point(408, 327);
			this->label21->Name = L"label21";
			this->label21->Size = System::Drawing::Size(69, 13);
			this->label21->TabIndex = 21;
			this->label21->Text = L"Mobilnummer";
			// 
			// label22
			// 
			this->label22->AutoSize = true;
			this->label22->Location = System::Drawing::Point(400, 358);
			this->label22->Name = L"label22";
			this->label22->Size = System::Drawing::Size(77, 13);
			this->label22->TabIndex = 22;
			this->label22->Text = L"E-Mail Adresse";
			// 
			// textBox1
			// 
			this->textBox1->Enabled = false;
			this->textBox1->Location = System::Drawing::Point(124, 46);
			this->textBox1->Name = L"textBox1";
			this->textBox1->Size = System::Drawing::Size(226, 20);
			this->textBox1->TabIndex = 23;
			this->textBox1->TextChanged += gcnew System::EventHandler(this, &AnzeigenForm::textBox1_TextChanged);
			// 
			// textBox2
			// 
			this->textBox2->Enabled = false;
			this->textBox2->Location = System::Drawing::Point(124, 79);
			this->textBox2->Name = L"textBox2";
			this->textBox2->Size = System::Drawing::Size(226, 20);
			this->textBox2->TabIndex = 24;
			this->textBox2->TextChanged += gcnew System::EventHandler(this, &AnzeigenForm::textBox2_TextChanged);
			// 
			// textBox3
			// 
			this->textBox3->Enabled = false;
			this->textBox3->Location = System::Drawing::Point(124, 110);
			this->textBox3->Name = L"textBox3";
			this->textBox3->Size = System::Drawing::Size(226, 20);
			this->textBox3->TabIndex = 25;
			this->textBox3->TextChanged += gcnew System::EventHandler(this, &AnzeigenForm::textBox3_TextChanged);
			// 
			// textBox4
			// 
			this->textBox4->Enabled = false;
			this->textBox4->Location = System::Drawing::Point(124, 141);
			this->textBox4->Name = L"textBox4";
			this->textBox4->Size = System::Drawing::Size(226, 20);
			this->textBox4->TabIndex = 26;
			this->textBox4->TextChanged += gcnew System::EventHandler(this, &AnzeigenForm::textBox4_TextChanged);
			// 
			// textBox5
			// 
			this->textBox5->Enabled = false;
			this->textBox5->Location = System::Drawing::Point(124, 172);
			this->textBox5->Name = L"textBox5";
			this->textBox5->Size = System::Drawing::Size(226, 20);
			this->textBox5->TabIndex = 27;
			this->textBox5->TextChanged += gcnew System::EventHandler(this, &AnzeigenForm::textBox5_TextChanged);
			// 
			// textBox6
			// 
			this->textBox6->Enabled = false;
			this->textBox6->Location = System::Drawing::Point(123, 200);
			this->textBox6->Name = L"textBox6";
			this->textBox6->Size = System::Drawing::Size(226, 20);
			this->textBox6->TabIndex = 28;
			this->textBox6->TextChanged += gcnew System::EventHandler(this, &AnzeigenForm::textBox6_TextChanged);
			// 
			// button2
			// 
			this->button2->Location = System::Drawing::Point(556, 426);
			this->button2->Name = L"button2";
			this->button2->Size = System::Drawing::Size(70, 24);
			this->button2->TabIndex = 29;
			this->button2->Text = L"Weiter";
			this->button2->UseVisualStyleBackColor = true;
			this->button2->Click += gcnew System::EventHandler(this, &AnzeigenForm::button2_Click);
			// 
			// button3
			// 
			this->button3->Location = System::Drawing::Point(480, 426);
			this->button3->Name = L"button3";
			this->button3->Size = System::Drawing::Size(70, 24);
			this->button3->TabIndex = 30;
			this->button3->Text = L"Zur�ck";
			this->button3->UseVisualStyleBackColor = true;
			this->button3->Click += gcnew System::EventHandler(this, &AnzeigenForm::button3_Click);
			// 
			// button4
			// 
			this->button4->Location = System::Drawing::Point(239, 426);
			this->button4->Name = L"button4";
			this->button4->Size = System::Drawing::Size(111, 24);
			this->button4->TabIndex = 31;
			this->button4->Text = L"Datensatz �ndern";
			this->button4->UseVisualStyleBackColor = true;
			this->button4->Click += gcnew System::EventHandler(this, &AnzeigenForm::button4_Click);
			// 
			// textBox7
			// 
			this->textBox7->Enabled = false;
			this->textBox7->Location = System::Drawing::Point(123, 231);
			this->textBox7->Name = L"textBox7";
			this->textBox7->Size = System::Drawing::Size(226, 20);
			this->textBox7->TabIndex = 32;
			this->textBox7->TextChanged += gcnew System::EventHandler(this, &AnzeigenForm::textBox7_TextChanged);
			// 
			// textBox8
			// 
			this->textBox8->Enabled = false;
			this->textBox8->Location = System::Drawing::Point(123, 262);
			this->textBox8->Name = L"textBox8";
			this->textBox8->Size = System::Drawing::Size(226, 20);
			this->textBox8->TabIndex = 33;
			this->textBox8->TextChanged += gcnew System::EventHandler(this, &AnzeigenForm::textBox8_TextChanged);
			// 
			// textBox9
			// 
			this->textBox9->Enabled = false;
			this->textBox9->Location = System::Drawing::Point(124, 293);
			this->textBox9->Name = L"textBox9";
			this->textBox9->Size = System::Drawing::Size(226, 20);
			this->textBox9->TabIndex = 34;
			this->textBox9->TextChanged += gcnew System::EventHandler(this, &AnzeigenForm::textBox9_TextChanged);
			// 
			// textBox10
			// 
			this->textBox10->Enabled = false;
			this->textBox10->Location = System::Drawing::Point(123, 324);
			this->textBox10->Name = L"textBox10";
			this->textBox10->Size = System::Drawing::Size(226, 20);
			this->textBox10->TabIndex = 35;
			this->textBox10->TextChanged += gcnew System::EventHandler(this, &AnzeigenForm::textBox10_TextChanged);
			// 
			// textBox11
			// 
			this->textBox11->Enabled = false;
			this->textBox11->Location = System::Drawing::Point(124, 358);
			this->textBox11->Name = L"textBox11";
			this->textBox11->Size = System::Drawing::Size(226, 20);
			this->textBox11->TabIndex = 36;
			this->textBox11->TextChanged += gcnew System::EventHandler(this, &AnzeigenForm::textBox11_TextChanged);
			// 
			// textBox12
			// 
			this->textBox12->Enabled = false;
			this->textBox12->Location = System::Drawing::Point(483, 43);
			this->textBox12->Name = L"textBox12";
			this->textBox12->Size = System::Drawing::Size(226, 20);
			this->textBox12->TabIndex = 37;
			this->textBox12->TextChanged += gcnew System::EventHandler(this, &AnzeigenForm::textBox12_TextChanged);
			// 
			// textBox13
			// 
			this->textBox13->Enabled = false;
			this->textBox13->Location = System::Drawing::Point(483, 76);
			this->textBox13->Name = L"textBox13";
			this->textBox13->Size = System::Drawing::Size(225, 20);
			this->textBox13->TabIndex = 38;
			this->textBox13->TextChanged += gcnew System::EventHandler(this, &AnzeigenForm::textBox13_TextChanged);
			// 
			// textBox14
			// 
			this->textBox14->Enabled = false;
			this->textBox14->Location = System::Drawing::Point(483, 107);
			this->textBox14->Name = L"textBox14";
			this->textBox14->Size = System::Drawing::Size(225, 20);
			this->textBox14->TabIndex = 39;
			this->textBox14->TextChanged += gcnew System::EventHandler(this, &AnzeigenForm::textBox15_TextChanged);
			// 
			// textBox15
			// 
			this->textBox15->Enabled = false;
			this->textBox15->Location = System::Drawing::Point(483, 138);
			this->textBox15->Name = L"textBox15";
			this->textBox15->Size = System::Drawing::Size(225, 20);
			this->textBox15->TabIndex = 40;
			this->textBox15->TextChanged += gcnew System::EventHandler(this, &AnzeigenForm::textBox15_TextChanged);
			// 
			// textBox16
			// 
			this->textBox16->Enabled = false;
			this->textBox16->Location = System::Drawing::Point(483, 169);
			this->textBox16->Name = L"textBox16";
			this->textBox16->Size = System::Drawing::Size(225, 20);
			this->textBox16->TabIndex = 41;
			this->textBox16->TextChanged += gcnew System::EventHandler(this, &AnzeigenForm::textBox16_TextChanged);
			// 
			// textBox17
			// 
			this->textBox17->Enabled = false;
			this->textBox17->Location = System::Drawing::Point(483, 200);
			this->textBox17->Name = L"textBox17";
			this->textBox17->Size = System::Drawing::Size(225, 20);
			this->textBox17->TabIndex = 42;
			this->textBox17->TextChanged += gcnew System::EventHandler(this, &AnzeigenForm::textBox17_TextChanged);
			// 
			// textBox18
			// 
			this->textBox18->Enabled = false;
			this->textBox18->Location = System::Drawing::Point(484, 231);
			this->textBox18->Name = L"textBox18";
			this->textBox18->Size = System::Drawing::Size(225, 20);
			this->textBox18->TabIndex = 43;
			this->textBox18->TextChanged += gcnew System::EventHandler(this, &AnzeigenForm::textBox18_TextChanged);
			// 
			// textBox19
			// 
			this->textBox19->Enabled = false;
			this->textBox19->Location = System::Drawing::Point(483, 262);
			this->textBox19->Name = L"textBox19";
			this->textBox19->Size = System::Drawing::Size(225, 20);
			this->textBox19->TabIndex = 44;
			this->textBox19->TextChanged += gcnew System::EventHandler(this, &AnzeigenForm::textBox19_TextChanged);
			// 
			// textBox20
			// 
			this->textBox20->Enabled = false;
			this->textBox20->Location = System::Drawing::Point(483, 293);
			this->textBox20->Name = L"textBox20";
			this->textBox20->Size = System::Drawing::Size(225, 20);
			this->textBox20->TabIndex = 45;
			this->textBox20->TextChanged += gcnew System::EventHandler(this, &AnzeigenForm::textBox20_TextChanged);
			// 
			// textBox21
			// 
			this->textBox21->Enabled = false;
			this->textBox21->Location = System::Drawing::Point(483, 324);
			this->textBox21->Name = L"textBox21";
			this->textBox21->Size = System::Drawing::Size(225, 20);
			this->textBox21->TabIndex = 46;
			this->textBox21->TextChanged += gcnew System::EventHandler(this, &AnzeigenForm::textBox21_TextChanged);
			// 
			// textBox22
			// 
			this->textBox22->Enabled = false;
			this->textBox22->Location = System::Drawing::Point(483, 358);
			this->textBox22->Name = L"textBox22";
			this->textBox22->Size = System::Drawing::Size(225, 20);
			this->textBox22->TabIndex = 47;
			this->textBox22->TextChanged += gcnew System::EventHandler(this, &AnzeigenForm::textBox22_TextChanged);
			// 
			// button5
			// 
			this->button5->Location = System::Drawing::Point(122, 426);
			this->button5->Name = L"button5";
			this->button5->Size = System::Drawing::Size(111, 24);
			this->button5->TabIndex = 48;
			this->button5->Text = L"Datensatz l�schen";
			this->button5->UseVisualStyleBackColor = true;
			this->button5->Click += gcnew System::EventHandler(this, &AnzeigenForm::button5_Click);
			// 
			// button6
			// 
			this->button6->Location = System::Drawing::Point(356, 426);
			this->button6->Name = L"button6";
			this->button6->Size = System::Drawing::Size(98, 24);
			this->button6->TabIndex = 49;
			this->button6->Text = L"�bernehmen";
			this->button6->UseVisualStyleBackColor = true;
			this->button6->Click += gcnew System::EventHandler(this, &AnzeigenForm::button6_Click);
			// 
			// label23
			// 
			this->label23->AutoSize = true;
			this->label23->Location = System::Drawing::Point(34, 18);
			this->label23->Name = L"label23";
			this->label23->Size = System::Drawing::Size(82, 13);
			this->label23->TabIndex = 50;
			this->label23->Text = L"Eintragsnummer";
			// 
			// textBox23
			// 
			this->textBox23->Location = System::Drawing::Point(124, 18);
			this->textBox23->Name = L"textBox23";
			this->textBox23->Size = System::Drawing::Size(73, 20);
			this->textBox23->TabIndex = 51;
			this->textBox23->TextChanged += gcnew System::EventHandler(this, &AnzeigenForm::textBox23_TextChanged);
			// 
			// label24
			// 
			this->label24->AutoSize = true;
			this->label24->Location = System::Drawing::Point(398, 18);
			this->label24->Name = L"label24";
			this->label24->Size = System::Drawing::Size(73, 13);
			this->label24->TabIndex = 52;
			this->label24->Text = L"Anzahl Treffer";
			// 
			// label25
			// 
			this->label25->AutoSize = true;
			this->label25->Location = System::Drawing::Point(483, 17);
			this->label25->Name = L"label25";
			this->label25->Size = System::Drawing::Size(0, 13);
			this->label25->TabIndex = 53;
			// 
			// label26
			// 
			this->label26->AutoSize = true;
			this->label26->Location = System::Drawing::Point(485, 18);
			this->label26->Name = L"label26";
			this->label26->Size = System::Drawing::Size(41, 13);
			this->label26->TabIndex = 54;
			this->label26->Text = L"label26";
			this->label26->Click += gcnew System::EventHandler(this, &AnzeigenForm::label26_Click);
			// 
			// label27
			// 
			this->label27->AutoSize = true;
			this->label27->Location = System::Drawing::Point(100, 400);
			this->label27->Name = L"label27";
			this->label27->Size = System::Drawing::Size(57, 13);
			this->label27->TabIndex = 22;
			this->label27->Text = L"*Pflichtfeld";
			// 
			// button7
			// 
			this->button7->Location = System::Drawing::Point(13, 426);
			this->button7->Name = L"button7";
			this->button7->Size = System::Drawing::Size(75, 23);
			this->button7->TabIndex = 55;
			this->button7->Text = L"Hilfe";
			this->button7->UseVisualStyleBackColor = true;
			this->button7->Click += gcnew System::EventHandler(this, &AnzeigenForm::button7_Click);
			// 
			// AnzeigenForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(734, 462);
			this->Controls->Add(this->button7);
			this->Controls->Add(this->label26);
			this->Controls->Add(this->label25);
			this->Controls->Add(this->label24);
			this->Controls->Add(this->textBox23);
			this->Controls->Add(this->label23);
			this->Controls->Add(this->button6);
			this->Controls->Add(this->button5);
			this->Controls->Add(this->textBox22);
			this->Controls->Add(this->textBox21);
			this->Controls->Add(this->textBox20);
			this->Controls->Add(this->textBox19);
			this->Controls->Add(this->textBox18);
			this->Controls->Add(this->textBox17);
			this->Controls->Add(this->textBox16);
			this->Controls->Add(this->textBox15);
			this->Controls->Add(this->textBox14);
			this->Controls->Add(this->textBox13);
			this->Controls->Add(this->textBox12);
			this->Controls->Add(this->textBox11);
			this->Controls->Add(this->textBox10);
			this->Controls->Add(this->textBox9);
			this->Controls->Add(this->textBox8);
			this->Controls->Add(this->textBox7);
			this->Controls->Add(this->button4);
			this->Controls->Add(this->button3);
			this->Controls->Add(this->button2);
			this->Controls->Add(this->textBox6);
			this->Controls->Add(this->textBox5);
			this->Controls->Add(this->textBox4);
			this->Controls->Add(this->textBox3);
			this->Controls->Add(this->textBox2);
			this->Controls->Add(this->textBox1);
			this->Controls->Add(this->label27);
			this->Controls->Add(this->label22);
			this->Controls->Add(this->label21);
			this->Controls->Add(this->label20);
			this->Controls->Add(this->label19);
			this->Controls->Add(this->label18);
			this->Controls->Add(this->label17);
			this->Controls->Add(this->label16);
			this->Controls->Add(this->label15);
			this->Controls->Add(this->label14);
			this->Controls->Add(this->label13);
			this->Controls->Add(this->label12);
			this->Controls->Add(this->label11);
			this->Controls->Add(this->label10);
			this->Controls->Add(this->label9);
			this->Controls->Add(this->label8);
			this->Controls->Add(this->label7);
			this->Controls->Add(this->label6);
			this->Controls->Add(this->label5);
			this->Controls->Add(this->label4);
			this->Controls->Add(this->label3);
			this->Controls->Add(this->label2);
			this->Controls->Add(this->label1);
			this->Controls->Add(this->button1);
			this->Location = System::Drawing::Point(300, 300);
			this->MaximumSize = System::Drawing::Size(750, 500);
			this->MinimumSize = System::Drawing::Size(667, 402);
			this->Name = L"AnzeigenForm";
			this->StartPosition = System::Windows::Forms::FormStartPosition::CenterParent;
			
			if (bearbeitenhilf == 1)
			{
				this->Text = L"Datens�tze eingeben";
			}
			else if (bearbeitenhilf == 2)
			{
				this->Text = L"Datens�tze anzeigen";
			}
			else if (bearbeitenhilf == 3)
			{
				this->Text = L"Datensatz �ndern";
			}
			else if (bearbeitenhilf == 4)
			{
				this->Text = L"Suchergebnisse";
			}

			this->Load += gcnew System::EventHandler(this, &AnzeigenForm::AnzeigenForm_Load);
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	public: System::Void AnzeigenForm_Load(System::Object^  sender, System::EventArgs^  e);
	
	public: System::Void button4_Click(System::Object^  sender, System::EventArgs^  e);			//�ndern

	public: System::Void button6_Click(System::Object^  sender, System::EventArgs^  e);			//�bernehmen

	public: System::Void button2_Click(System::Object^  sender, System::EventArgs^  e);			//Weiter

	public: System::Void button3_Click(System::Object^  sender, System::EventArgs^  e);			//Zur�ck

	public: System::Void textBox23_TextChanged(System::Object^  sender, System::EventArgs^  e);	//Wert f�r Nummer eingeben

	public: System::Void button5_Click(System::Object^  sender, System::EventArgs^  e);

	private: System::Void button1_Click_1(System::Object^  sender, System::EventArgs^  e) {
		extern bool meldung;
		extern bool meldung2;
		
		// Kontrolle, ob ein Feld beschrieben ist. Sind Felder beschrieben und man dr�ckt trotzdem auf "Schlie�en", bekommt der Benutzer einen Warnhinweis, dass die Daten verloren gehen.
		if ((String::IsNullOrEmpty(textBox1->Text) && String::IsNullOrEmpty(textBox2->Text) && String::IsNullOrEmpty(textBox3->Text) 
			&& String::IsNullOrEmpty(textBox4->Text) && String::IsNullOrEmpty(textBox5->Text) && String::IsNullOrEmpty(textBox6->Text) 
			&& String::IsNullOrEmpty(textBox7->Text) && String::IsNullOrEmpty(textBox8->Text) && String::IsNullOrEmpty(textBox9->Text) 
			&& String::IsNullOrEmpty(textBox10->Text) && String::IsNullOrEmpty(textBox11->Text) && String::IsNullOrEmpty(textBox12->Text) 
			&& String::IsNullOrEmpty(textBox13->Text) && String::IsNullOrEmpty(textBox14->Text) && String::IsNullOrEmpty(textBox15->Text) 
			&& String::IsNullOrEmpty(textBox16->Text) && String::IsNullOrEmpty(textBox17->Text) && String::IsNullOrEmpty(textBox18->Text) 
			&& String::IsNullOrEmpty(textBox19->Text) && String::IsNullOrEmpty(textBox20->Text) && String::IsNullOrEmpty(textBox21->Text) 
			&& String::IsNullOrEmpty(textBox22->Text)) || meldung == true)
		{
			Close();
		}
		else if (meldung2 == false && MessageBox::Show(					//Warnhinweis f�r das "Eingeben"-Fenster
			"M�chten Sie die Eingabe abbrechen? Falls JA, gehen die get�tigten Eingaben verloren",
			"Daten gehen verloren!", MessageBoxButtons::YesNo,
			MessageBoxIcon::Question) == ::System::Windows::Forms::DialogResult::Yes)
		{
			Close();
		}
		else if (meldung2 == true && MessageBox::Show(					//Warnhinweis f�r das "�ndern"-Fenster
			"M�chten Sie die �nderung abbrechen? Falls JA, gehen die get�tigten �nderungen verloren",
			"�nderungen gehen verloren!", MessageBoxButtons::YesNo,
			MessageBoxIcon::Question) == ::System::Windows::Forms::DialogResult::Yes)
		{
			Close();
			meldung = true;
		}
	
	}

			
	void bearbeitenumschaltenumschalten(int bearbeiten) //Funktion zum Umschalten der Textboxen und Buttons auf enabled/disabled & Visible. Verschiedene Zust�nde brauchen unterschiedliche Buttons, daher diese Funktion
			 {
				 extern int anzahl;
				 if (bearbeiten == 1) //Eingeben-Fenster
				 {
					 textBox1->Enabled = true;
					 textBox2->Enabled = true;
					 textBox3->Enabled = true;
					 textBox4->Enabled = true;
					 textBox5->Enabled = true;
					 textBox6->Enabled = true;
					 textBox7->Enabled = true;
					 textBox8->Enabled = true;
					 textBox9->Enabled = true;
					 textBox10->Enabled = true;
					 textBox11->Enabled = true;
					 textBox12->Enabled = true;
					 textBox13->Enabled = true;
					 textBox14->Enabled = true;
					 textBox15->Enabled = true;
					 textBox16->Enabled = true;
					 textBox17->Enabled = true;
					 textBox18->Enabled = true;
					 textBox19->Enabled = true;
					 textBox20->Enabled = true;
					 textBox21->Enabled = true;
					 textBox22->Enabled = true;
					 textBox23->Enabled = false;

					 button1->Visible = false;	//In Eingabe kein normales Schlie�en m�glich
					 button2->Visible = false;	//In der Eingabe-Funktion werden die Buttons "L�schen" und "�ndern" nicht gebraucht
					 button3->Visible = false;
					 button4->Visible = false;	//In der Eingabe-Funktion werden die Buttons "L�schen" und "�ndern" nicht gebraucht
					 button5->Visible = false;

				 }
				 else if (bearbeiten == 2) //Anzeigen-Fenster
				 {
					 textBox1->Enabled = false;
					 textBox2->Enabled = false;
					 textBox3->Enabled = false;
					 textBox4->Enabled = false;
					 textBox5->Enabled = false;
					 textBox6->Enabled = false;
					 textBox7->Enabled = false;
					 textBox8->Enabled = false;
					 textBox9->Enabled = false;
					 textBox10->Enabled = false;
					 textBox11->Enabled = false;
					 textBox12->Enabled = false;
					 textBox13->Enabled = false;
					 textBox14->Enabled = false;
					 textBox15->Enabled = false;
					 textBox16->Enabled = false;
					 textBox17->Enabled = false;
					 textBox18->Enabled = false;
					 textBox19->Enabled = false;
					 textBox20->Enabled = false;
					 textBox21->Enabled = false;
					 textBox22->Enabled = false;
					 textBox23->Enabled = false;

					 if (anzahl == 0){
						button4->Visible = false;
						button5->Visible = false;
					 }
					 else{
						 button6->Visible = false;
						 button2->Visible = true;
						 button3->Visible = true;
						 button4->Visible = true;
						 button5->Visible = true;
					 }
				 }
				 else if (bearbeiten == 3) //�ndern-Fenster
				 {
					 textBox1->Enabled = true;
					 textBox2->Enabled = true;
					 textBox3->Enabled = true;
					 textBox4->Enabled = true;
					 textBox5->Enabled = true;
					 textBox6->Enabled = true;
					 textBox7->Enabled = true;
					 textBox8->Enabled = true;
					 textBox9->Enabled = true;
					 textBox10->Enabled = true;
					 textBox11->Enabled = true;
					 textBox12->Enabled = true;
					 textBox13->Enabled = true;
					 textBox14->Enabled = true;
					 textBox15->Enabled = true;
					 textBox16->Enabled = true;
					 textBox17->Enabled = true;
					 textBox18->Enabled = true;
					 textBox19->Enabled = true;
					 textBox20->Enabled = true;
					 textBox21->Enabled = true;
					 textBox22->Enabled = true;
					 textBox23->Enabled = false;

					 button6->Visible = true;
					 button2->Visible = false;	//In der Eingabe-Funktion werden die Buttons "L�schen" und "�ndern nicht gebraucht
					 button3->Visible = false;
					 button4->Visible = false; //�ndernButton
					 button5->Visible = false;  //L�schenButton
					 
				 }
				 else if (bearbeiten == 4) //Suchen-Fenster
				 {
					 textBox1->Enabled = false;
					 textBox2->Enabled = false;
					 textBox3->Enabled = false;
					 textBox4->Enabled = false;
					 textBox5->Enabled = false;
					 textBox6->Enabled = false;
					 textBox7->Enabled = false;
					 textBox8->Enabled = false;
					 textBox9->Enabled = false;
					 textBox10->Enabled = false;
					 textBox11->Enabled = false;
					 textBox12->Enabled = false;
					 textBox13->Enabled = false;
					 textBox14->Enabled = false;
					 textBox15->Enabled = false;
					 textBox16->Enabled = false;
					 textBox17->Enabled = false;
					 textBox18->Enabled = false;
					 textBox19->Enabled = false;
					 textBox20->Enabled = false;
					 textBox21->Enabled = false;
					 textBox22->Enabled = false;
					 textBox23->Enabled = false;

					 button6->Visible = false;
					 button2->Visible = true;	
					 button3->Visible = true;
					 button4->Visible = true;
					 button5->Visible = true;
				 }
			 }


//---------------- Enable �bernehmen-Button = 1, sobald etwas ge�ndert wurde in irgendeinem Feld
private: System::Void textBox1_TextChanged(System::Object^  sender, System::EventArgs^  e) {
	button6->Enabled = true;
}
private: System::Void textBox2_TextChanged(System::Object^  sender, System::EventArgs^  e) {
	button6->Enabled = true;
}
private: System::Void textBox3_TextChanged(System::Object^  sender, System::EventArgs^  e) {
	button6->Enabled = true;
}
private: System::Void textBox4_TextChanged(System::Object^  sender, System::EventArgs^  e) {
	 button6->Enabled = true;
}
private: System::Void textBox5_TextChanged(System::Object^  sender, System::EventArgs^  e) {
	  button6->Enabled = true;
}
private: System::Void textBox6_TextChanged(System::Object^  sender, System::EventArgs^  e) {
	   button6->Enabled = true;
}
private: System::Void textBox7_TextChanged(System::Object^  sender, System::EventArgs^  e) {
	button6->Enabled = true;
}
private: System::Void textBox8_TextChanged(System::Object^  sender, System::EventArgs^  e) {
	button6->Enabled = true;
}
private: System::Void textBox9_TextChanged(System::Object^  sender, System::EventArgs^  e) {
	button6->Enabled = true;
}
private: System::Void textBox10_TextChanged(System::Object^  sender, System::EventArgs^  e) {
	button6->Enabled = true;
}
private: System::Void textBox11_TextChanged(System::Object^  sender, System::EventArgs^  e) {
	button6->Enabled = true;
}
private: System::Void textBox12_TextChanged(System::Object^  sender, System::EventArgs^  e) {
	button6->Enabled = true;
}
private: System::Void textBox13_TextChanged(System::Object^  sender, System::EventArgs^  e) {
	button6->Enabled = true;
}
private: System::Void textBox14_TextChanged(System::Object^  sender, System::EventArgs^  e) {
	button6->Enabled = true;
}
private: System::Void textBox15_TextChanged(System::Object^  sender, System::EventArgs^  e) {
	button6->Enabled = true;
}
private: System::Void textBox16_TextChanged(System::Object^  sender, System::EventArgs^  e) {
	button6->Enabled = true;
}
private: System::Void textBox17_TextChanged(System::Object^  sender, System::EventArgs^  e) {
	button6->Enabled = true;
}
private: System::Void textBox18_TextChanged(System::Object^  sender, System::EventArgs^  e) {
	button6->Enabled = true;
}
private: System::Void textBox19_TextChanged(System::Object^  sender, System::EventArgs^  e) {
	button6->Enabled = true;
}
private: System::Void textBox20_TextChanged(System::Object^  sender, System::EventArgs^  e) {
	button6->Enabled = true;
}
private: System::Void textBox21_TextChanged(System::Object^  sender, System::EventArgs^  e) {
	button6->Enabled = true;
}
private: System::Void textBox22_TextChanged(System::Object^  sender, System::EventArgs^  e) {
	button6->Enabled = true;
}
//-----------------------------------------------------------------------------------------------


private: System::Void label26_Click(System::Object^  sender, System::EventArgs^  e) {
}


//----Eventhandler Hilfe-Button --------------- Ruft, entsprechend des Fensters von dem er gedr�ckt wird, das entsprechende Hilfe-Fenster auf
private: System::Void button7_Click(System::Object^  sender, System::EventArgs^  e)
	{
		if (bearbeitenhilf == 1) 
		{
			HilfeForm^HILF = gcnew HilfeForm(1);   // Objekt von HilfeForm erstellen mit bearbteiten = 1 (Eingebenfenster)
			HILF->ShowDialog(this);
		}
		else if (bearbeitenhilf == 2)
		{
			HilfeForm^HILF = gcnew HilfeForm(2);   // Objekt von HilfeForm erstellen mit bearbtiten = 2 (Anzeigenfenster)
			HILF->ShowDialog(this);
		}
		else if (bearbeitenhilf == 3)
		{
			HilfeForm^HILF = gcnew HilfeForm(3);   // Objekt von HilfeForm erstellen mit bearbtiten = 3 (�ndernfenster)
			HILF->ShowDialog(this);
		}
		else if (bearbeitenhilf == 4)
		{
			HilfeForm^HILF = gcnew HilfeForm(4);   // Objekt von HilfeForm erstellen mit bearbtiten = 4 (Suchenfenster)
			HILF->ShowDialog(this);
		}
	}
};


}
