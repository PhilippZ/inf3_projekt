#include <stdio.h>
#include <conio.h>
#include <string.h>

#include "Datentypen.h"

//Auf diese Datentypen bzw. Variablen wird von allen Unterfunktionen zugegriffen, weshalb sie seperat und global deklariert werden

STUDENT Eintrag[150];


int nummer;				//nummer f�r durchklicken durch Datenbank in der Anzeige
int nummer2;			//nummer f�r durchklicken durch Datenbank in der Suche (Sucharray ist anders als Anzeigearray)
int anzahl;				//Anzahl Eintr�ge in der Datenbank
int Ergebnisanzahl;		//Anzahl an Suchergebnissen
int ha[150];			//Hilfsarray in das die gefundenen Eintragsnummer geschrieben werden 
int Ergebnisse[150];	//Ergebnisarray in das die gefundenen Eintragsnummer geschrieben werden
int la;					//Laufvariable f�r hs[150]
bool meldung;			//Merkervariable, zeigt dem Programm an in welchen Fenster man sich befindet und ob es beim dr�cken des "Schlie�en"-Buttons eine Warnmeldung geben soll
bool meldung2;			//Merkervariable, wie "bool meldung", jedoch extra f�r das Fenster "Datensatz �ndern", damit eine Textbox mit individuellen Text ge�ffnet wird

int zaehlen()			//Funktion zum Z�hlen der angegebenen Datens�tze
{		

	int i = 0;			//Z�hlervariable
	extern int anzahl;
	extern int nummer;
	extern STUDENT Eintrag[150];
	anzahl = 0;

	while (i < 150){

		if (Eintrag[i].Dateneingegeben == 1){		//Pr�fen ob die relevante Variable auf eins gesetzt wurde. Dies geschieht, wenn mindestens ein Attribut eingegeben wird
			anzahl++;								//Wenn Ja, Anzahl um 1 erh�hen
		};
		i++;			//N�chsten Eintrag pr�fen
	};
	return 0;

}
	

int einlesen()
{
	int size;
	zaehlen();
	FILE* fa = fopen("datenbank.dat", "rb");			//�ffnen der Datei
	if (fa != NULL){									//�berpr�fen ob Datei existiert
		fseek(fa, 0L, SEEK_END);						//berechnen der Filegr��e
		size = ftell(fa);
		fseek(fa, 0L, SEEK_SET);						//wichtig bei mehreren Studenten
		anzahl = size / sizeof(STUDENT);				//Einstellen des Z�hlers in dem Gr��e der Datei durch die Gr��e eines Structs student geteilt wird
		fread(Eintrag, sizeof(STUDENT), anzahl, fa);	//einlesen in eingabe in struct student Gr��e, zaehler mal, aus fa
		fclose(fa);										//schlie�en von file
		//printf("Datens\204tze geladen\n");
		return 2;
	}
	else{												// falls kein Datensatz gefunden
		//printf("KeSine Datens\204tze geladen\n");
		return 1;
	}


	

}