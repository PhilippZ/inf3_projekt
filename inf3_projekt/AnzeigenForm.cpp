#include "AnzeigenForm.h"
//#include"Globalevariablen.h"
#include "Datentypen.h"
#include <string.h>
using namespace INF3_Projekt;



//Eventhandler Laden von AnzeigeForm
System::Void INF3_Projekt::AnzeigenForm::AnzeigenForm_Load(System::Object^  sender, System::EventArgs^  e)	
{

	
	extern STUDENT Eintrag[150];	//Einbinden der externen Variablen (Hauptarray mit Eintr�gen, Laufvariable und Anzahl der Eintr�ge)
	extern int nummer;
	extern int anzahl; 
	extern int Ergebnisanzahl;

	extern int zaehlen();			//Vorweg z�hlen der vorhandenen Eintr�ge

	//--------------AUSGEBEN der Attribute------------------------- 
	if (bearbeitenhilf == 4)	//Wenn "Anzeigen-Form" als Suche ge�ffnet, dann soll die Anzahl der Suchtreffer ausgegeben, sonst "Anzahl der Eintr�ge insgesamt"
	{
		label26->Text = Convert::ToString(Ergebnisanzahl);
	}
	else
	{
		label26->Text = Convert::ToString(anzahl);
	};


	button6->Enabled = false;						//[Nur f�r �ndern-Funktion] ... solange kein Feld bearbeitet wurde, kann "�bernehmen" nicht gedr�ckt werden
	

	textBox23->Text = Convert::ToString(nummer+1);	//Ausgabe der korrekten Eintragsnummer (+1 wegen Z�hlweise von Null)
	
	textBox1->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Studentname.Name));		//NAME					umwandeln von Char array in System String
	textBox2->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Studentname.Vorname));	//VORNAME				umwandeln von Char array in System String
	
	if (Eintrag[nummer].Studentname.Matrikelnummer != 0){								//Damit nicht "0" in den Feldern angezeigt wird, wenn nichts eingetragen wird
		textBox3->Text = Convert::ToString(Eintrag[nummer].Studentname.Matrikelnummer); //MATRIKELNUMMER
	}
	else{
		textBox3->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(""));
	};



	if (Eintrag[nummer].Studentname.Jahr!= 0){											//Damit nicht "0" in den Feldern angezeigt wird, wenn nichts eingetragen wird
		textBox4->Text = Convert::ToString(Eintrag[nummer].Studentname.Jahr);			//STUDIENBEGINN(Jahr)
	}
	else{
		textBox4->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(""));
	};

	textBox5->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Studentname.festnetz));			//FESTNETZNUMMER	umwandeln von Char array in System String
	
	textBox6->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Studentname.mobil));				//MOBILNUMMER		umwandeln von Char array in System String
	textBox7->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Studentname.email));				//EMAIL				umwandeln von Char array in System String
	textBox8->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Studentadresse.Strasse));		//STRA�E			umwandeln von Char array in System String
	textBox9->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Studentadresse.Hausnummer));		//Hausnummer		umwandeln von Char array in System String
	
	if (Eintrag[nummer].Studentadresse.Plz != 0){										//Damit nicht "0" in den Feldern angezeigt wird, wenn nichts eingetragen wird
		textBox10->Text = Convert::ToString(Eintrag[nummer].Studentadresse.Plz);		//PLZ
	}
	else{
		textBox10->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(""));
	};																


	textBox11->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Studentadresse.Stadt));			//STADT				umwandeln von Char array in System String
	textBox12->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Firmaname));					//Px_PARTNER		umwandeln von Char array in System String
	textBox13->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Firmaadresse.PxStrasse));		//STRA�E			umwandeln von Char array in System String
	textBox14->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Firmaadresse.PxHausnummer));	//NUMMER			umwandeln von Char array in System String
	
	if (Eintrag[nummer].Firmaadresse.PxPlz != 0){										//Damit nicht "0" in den Feldern angezeigt wird, wenn nichts eingetragen wird
		textBox15->Text = Convert::ToString(Eintrag[nummer].Firmaadresse.PxPlz);		//PX_ADRESSE
	}
	else{
		textBox15->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(""));
	};	
	
	textBox16->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Firmaadresse.PxStadt));			//STADT						umwandeln von Char array in System String
	textBox17->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Firmabetreuer.PxName));			//BETREUER	Name			umwandeln von Char array in System String
	textBox18->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Firmabetreuer.PxVorname));		//BETREUER 	Vorname
	textBox19->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Firmabetreuer.PxPosition));		//BETREUER	Position		umwandeln von Char array in System String
	textBox20->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Firmabetreuer.Pxfestnetz));		//BETREUER 	Festnetz
	textBox21->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Firmabetreuer.Pxmobil));		//BETREUER 	Mobilnummer
	textBox22->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Firmabetreuer.Pxemail));		//BETREUER 	E-Mail									
	
	//--------------AUSGEBEN Ende----------------------------------
}  


//Eventhandler Weiter-Button
System::Void INF3_Projekt::AnzeigenForm::button2_Click(System::Object^  sender, System::EventArgs^  e) {	//Weiter-button

	//durchbl�ttern (weiter) der Attribute
	//extern STUDENT Eintrag[150];
	extern int nummer;
	extern STUDENT Eintrag[150];
	extern int anzahl;
	//extern int bearbeiten;
	extern int la;
	extern int ha[150];
	extern int Ergebnisanzahl;

	if (bearbeitenhilf != 4)
	{	//Wenn Anzeigen Fenster nicht als suche l�uft, dann normale "nummer"-Variable +1
		nummer++;
	}
	else if (bearbeitenhilf == 4)
	{   //Wenn Anzeigen Fenster als Suche l�uft, dann entsprechende Laufvariable erh�hen
		la++;
		nummer = ha[la];
	};

	if (nummer >anzahl-1 && bearbeitenhilf != 4)			//Verhindern der Navigation �ber die vorhandenen Daten hinaus (Anzeige)
	{
		nummer = 0;
	}
	else if (bearbeitenhilf == 4 && la >Ergebnisanzahl-1)	//Gleiche Funktion, hier aber f�r Suchergebnisse (Suche)
	{
		la = 0;
		nummer = ha[la];
	};

	//--------------AUSGEBEN der Attribute------------------------- 

	textBox23->Text = Convert::ToString(nummer + 1);
	textBox1->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Studentname.Name));//NAME					umwandeln von Char array in System String
	textBox2->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Studentname.Vorname));//VORNAME				umwandeln von Char array in System String

	if (Eintrag[nummer].Studentname.Matrikelnummer != 0){								//Damit nicht "0" in den Feldern angezeigt wird, wenn nichts eingetragen wird
		textBox3->Text = Convert::ToString(Eintrag[nummer].Studentname.Matrikelnummer); //MATRIKELNUMMER
	}
	else{
		textBox3->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(""));
	};



	if (Eintrag[nummer].Studentname.Jahr != 0){								//Damit nicht "0" in den Feldern angezeigt wird, wenn nichts eingetragen wird
		textBox4->Text = Convert::ToString(Eintrag[nummer].Studentname.Jahr); //STUDIENBEGINN(Jahr)
	}
	else{
		textBox4->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(""));
	};

	textBox5->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Studentname.festnetz));//FESTNETZNUMMER		umwandeln von Char array in System String

	textBox6->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Studentname.mobil));//MOBILNUMMER			umwandeln von Char array in System String
	textBox7->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Studentname.email));//EMAIL					umwandeln von Char array in System String
	textBox8->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Studentadresse.Strasse));//STRA�E			umwandeln von Char array in System String
	textBox9->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Studentadresse.Hausnummer));//Hausnummer	umwandeln von Char array in System String

	if (Eintrag[nummer].Studentadresse.Plz != 0){								//Damit nicht "0" in den Feldern angezeigt wird, wenn nichts eingetragen wird
		textBox10->Text = Convert::ToString(Eintrag[nummer].Studentadresse.Plz); //PLZ
	}
	else{
		textBox10->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(""));
	};


	textBox11->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Studentadresse.Stadt));//STADT				umwandeln von Char array in System String
	textBox12->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Firmaname));//Px_PARTNER					umwandeln von Char array in System String
	textBox13->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Firmaadresse.PxStrasse));//STRA�E			umwandeln von Char array in System String
	textBox14->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Firmaadresse.PxHausnummer));//NUMMER		umwandeln von Char array in System String

	if (Eintrag[nummer].Firmaadresse.PxPlz != 0){								//Damit nicht "0" in den Feldern angezeigt wird, wenn nichts eingetragen wird
		textBox15->Text = Convert::ToString(Eintrag[nummer].Firmaadresse.PxPlz); //PX_ADRESSE
	}
	else{
		textBox15->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(""));
	};

	textBox16->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Firmaadresse.PxStadt));//STADT				umwandeln von Char array in System String
	textBox17->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Firmabetreuer.PxName));//BETREUER			umwandeln von Char array in System String
	textBox18->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Firmabetreuer.PxVorname));//BETREUER 	
	textBox19->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Firmabetreuer.PxPosition));//BETREUER			umwandeln von Char array in System String
	textBox20->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Firmabetreuer.Pxfestnetz));//BETREUER 	
	textBox21->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Firmabetreuer.Pxmobil));//BETREUER 	
	textBox22->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Firmabetreuer.Pxemail));//BETREUER 			//AUSGABE aller Attribute (Schreiben in die TextBoxen)	//AUSGABE aller Attribute

	//--------------AUSGEBEN Ende----------------------------------

}


//Eventhandler Zur�ck-Button
System::Void INF3_Projekt::AnzeigenForm::button3_Click(System::Object^  sender, System::EventArgs^  e) {	//Zur�ck-button

	//durchbl�ttern (zur�ck) der Attribute

	extern int nummer;
	extern STUDENT Eintrag[150];
	extern int anzahl;
	extern int la;
	extern int ha[150];
	extern int Ergebnisanzahl;

	if (bearbeitenhilf != 4)
	{	//Wenn Anzeigen Fenster nicht als suche l�uft, dann normale "nummer"-Variable -1
		nummer--;					
	}
	else if (bearbeitenhilf == 4)
	{   //Wenn Anzeigen Fenster als Suche l�uft, dann entsprechende Laufvariable senken
		la--;
		nummer = ha[la];


	};
	if (nummer < 0 && bearbeitenhilf != 4)		//Verhindern der Navigation unter 1, bzw. 0
	{ 
		nummer = anzahl - 1;
	}
	else if (bearbeitenhilf == 4 && la < 0)		//gleiche Funktion, hier f�r Suche
	{
		la = Ergebnisanzahl-1;
		nummer = ha[la];
	}

	//--------------AUSGEBEN der Attribute------------------------- 
	textBox23->Text = Convert::ToString(nummer + 1);
	textBox1->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Studentname.Name));//NAME					umwandeln von Char array in System String
	textBox2->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Studentname.Vorname));//VORNAME				umwandeln von Char array in System String

	if (Eintrag[nummer].Studentname.Matrikelnummer != 0){								//Damit nicht "0" in den Feldern angezeigt wird, wenn nichts eingetragen wird
		textBox3->Text = Convert::ToString(Eintrag[nummer].Studentname.Matrikelnummer); //MATRIKELNUMMER
	}
	else{
		textBox3->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(""));
	};



	if (Eintrag[nummer].Studentname.Jahr != 0){								//Damit nicht "0" in den Feldern angezeigt wird, wenn nichts eingetragen wird
		textBox4->Text = Convert::ToString(Eintrag[nummer].Studentname.Jahr); //STUDIENBEGINN(Jahr)
	}
	else{
		textBox4->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(""));
	};

	textBox5->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Studentname.festnetz));//FESTNETZNUMMER		umwandeln von Char array in System String

	textBox6->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Studentname.mobil));//MOBILNUMMER			umwandeln von Char array in System String
	textBox7->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Studentname.email));//EMAIL					umwandeln von Char array in System String
	textBox8->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Studentadresse.Strasse));//STRA�E			umwandeln von Char array in System String
	textBox9->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Studentadresse.Hausnummer));//Hausnummer	umwandeln von Char array in System String

	if (Eintrag[nummer].Studentadresse.Plz != 0){								//Damit nicht "0" in den Feldern angezeigt wird, wenn nichts eingetragen wird
		textBox10->Text = Convert::ToString(Eintrag[nummer].Studentadresse.Plz); //PLZ
	}
	else{
		textBox10->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(""));
	};


	textBox11->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Studentadresse.Stadt));//STADT				umwandeln von Char array in System String
	textBox12->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Firmaname));//Px_PARTNER					umwandeln von Char array in System String
	textBox13->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Firmaadresse.PxStrasse));//STRA�E			umwandeln von Char array in System String
	textBox14->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Firmaadresse.PxHausnummer));//NUMMER		umwandeln von Char array in System String

	if (Eintrag[nummer].Firmaadresse.PxPlz != 0){								//Damit nicht "0" in den Feldern angezeigt wird, wenn nichts eingetragen wird
		textBox15->Text = Convert::ToString(Eintrag[nummer].Firmaadresse.PxPlz); //PX_ADRESSE
	}
	else{
		textBox15->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(""));
	};

	textBox16->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Firmaadresse.PxStadt));//STADT				umwandeln von Char array in System String
	textBox17->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Firmabetreuer.PxName));//BETREUER			umwandeln von Char array in System String
	textBox18->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Firmabetreuer.PxVorname));//BETREUER 	
	textBox19->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Firmabetreuer.PxPosition));//BETREUER			umwandeln von Char array in System String
	textBox20->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Firmabetreuer.Pxfestnetz));//BETREUER 	
	textBox21->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Firmabetreuer.Pxmobil));//BETREUER 	
	textBox22->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Firmabetreuer.Pxemail));//BETREUER 			//AUSGABE aller Attribute (Schreiben in die TextBoxen)	//AUSGABE aller Attribute										//AUSGABE aller Attribute
	
	//--------------AUSGEBEN Ende-------------------------------
}


//Eventhandler �ndern-Button
System::Void INF3_Projekt::AnzeigenForm::button4_Click(System::Object^  sender, System::EventArgs^  e) {	//    �NDERN

	

	extern int nummer;
	extern int anzahl;
	extern int zaehlen();
	extern STUDENT Eintrag[150];
	extern bool meldung;
	extern bool meldung2;
	// Hinweis an das Programm, dass man sich im "�ndern"-Fenster befindet --> individueller Warnhinweis beim Dr�cken des "Schlie�en"-Buttons
	meldung = false;
	meldung2 = true;
	
	//damit der aktuell angezeigte Eintrag bearbeitet wird, wird "nummer" �bergeben
	AnzeigenForm^ANZ = gcnew AnzeigenForm(3, nummer);   // Objekt von AnzeigenForm erstellen mit bearbtiten = false (--> Textboxen disabled)
	ANZ->ShowDialog();

	//-------Nach �nderungen: aktualisieren der Textboxen---------------------
	textBox23->Text = Convert::ToString(nummer + 1);
	textBox1->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Studentname.Name));//NAME					umwandeln von Char array in System String
	textBox2->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Studentname.Vorname));//VORNAME				umwandeln von Char array in System String

	if (Eintrag[nummer].Studentname.Matrikelnummer != 0){								//Damit nicht "0" in den Feldern angezeigt wird, wenn nichts eingetragen wird
		textBox3->Text = Convert::ToString(Eintrag[nummer].Studentname.Matrikelnummer); //MATRIKELNUMMER
	}
	else{
		textBox3->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(""));
	};



	if (Eintrag[nummer].Studentname.Jahr != 0){								//Damit nicht "0" in den Feldern angezeigt wird, wenn nichts eingetragen wird
		textBox4->Text = Convert::ToString(Eintrag[nummer].Studentname.Jahr); //STUDIENBEGINN(Jahr)
	}
	else{
		textBox4->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(""));
	};

	textBox5->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Studentname.festnetz));//FESTNETZNUMMER		umwandeln von Char array in System String

	textBox6->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Studentname.mobil));//MOBILNUMMER			umwandeln von Char array in System String
	textBox7->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Studentname.email));//EMAIL					umwandeln von Char array in System String
	textBox8->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Studentadresse.Strasse));//STRA�E			umwandeln von Char array in System String
	textBox9->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Studentadresse.Hausnummer));//Hausnummer	umwandeln von Char array in System String

	if (Eintrag[nummer].Studentadresse.Plz != 0){								//Damit nicht "0" in den Feldern angezeigt wird, wenn nichts eingetragen wird
		textBox10->Text = Convert::ToString(Eintrag[nummer].Studentadresse.Plz); //PLZ
	}
	else{
		textBox10->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(""));
	};


	textBox11->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Studentadresse.Stadt));//STADT				umwandeln von Char array in System String
	textBox12->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Firmaname));//Px_PARTNER					umwandeln von Char array in System String
	textBox13->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Firmaadresse.PxStrasse));//STRA�E			umwandeln von Char array in System String
	textBox14->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Firmaadresse.PxHausnummer));//NUMMER		umwandeln von Char array in System String

	if (Eintrag[nummer].Firmaadresse.PxPlz != 0){								//Damit nicht "0" in den Feldern angezeigt wird, wenn nichts eingetragen wird
		textBox15->Text = Convert::ToString(Eintrag[nummer].Firmaadresse.PxPlz); //PX_ADRESSE
	}
	else{
		textBox15->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(""));
	};

	textBox16->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Firmaadresse.PxStadt));//STADT				umwandeln von Char array in System String
	textBox17->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Firmabetreuer.PxName));//BETREUER			umwandeln von Char array in System String
	textBox18->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Firmabetreuer.PxVorname));//BETREUER 	
	textBox19->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Firmabetreuer.PxPosition));//BETREUER			umwandeln von Char array in System String
	textBox20->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Firmabetreuer.Pxfestnetz));//BETREUER 	
	textBox21->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Firmabetreuer.Pxmobil));//BETREUER 	
	textBox22->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Firmabetreuer.Pxemail));//BETREUER 			//AUSGABE aller Attribute (Schreiben in die TextBoxen)	//AUSGABE aller Attribute										//AUSGABE aller Attribute
	
	//------------------------------------------------------------------------


} 


//Eventhandler L�schen-Button
System::Void INF3_Projekt::AnzeigenForm::button5_Click(System::Object^  sender, System::EventArgs^  e)  //L�SCHEN
{
	extern int nummer;
	extern STUDENT Eintrag[150];
	extern int anzahl;
	extern int zaehlen();
	extern int Ergebnisanzahl;
	extern int Eingabemat;
	extern int ha[150];
	extern int la;

	int verschieben; //hilfsvariable zum Verschieben
	int verschiebensuch;
	
	if (MessageBox::Show(						//Sicherheitsabfrage zum Beenden
		"Soll der Datensatz wirklich gel�scht werden?",
		"Datensatz L�schen?", MessageBoxButtons::YesNo,
		MessageBoxIcon::Question) == ::System::Windows::Forms::DialogResult::Yes){

		//----Leeren des Eintrags----
		strcpy(Eintrag[nummer].Studentname.Name, "");		//String leeren
		strcpy(Eintrag[nummer].Studentname.Vorname, "");
		Eintrag[nummer].Studentname.Matrikelnummer = 0;		//Integer null setzen --> Werden bei der Ausgabe durch If-Bedingung nicht mehr angezeigt
		Eintrag[nummer].Studentname.Jahr = 0;
		strcpy(Eintrag[nummer].Studentname.festnetz, "");
		strcpy(Eintrag[nummer].Studentname.mobil, "");
		strcpy(Eintrag[nummer].Studentname.email, "");

		strcpy(Eintrag[nummer].Studentadresse.Strasse, "");
		strcpy(Eintrag[nummer].Studentadresse.Stadt, "");
		strcpy(Eintrag[nummer].Studentadresse.Hausnummer, "");
		Eintrag[nummer].Studentadresse.Plz = 0;

		strcpy(Eintrag[nummer].Firmaname, "");
		strcpy(Eintrag[nummer].Firmaadresse.PxStrasse, "");
		strcpy(Eintrag[nummer].Firmaadresse.PxStadt, "");
		strcpy(Eintrag[nummer].Firmaadresse.PxHausnummer, "");
		Eintrag[nummer].Firmaadresse.PxPlz = 0;

		strcpy(Eintrag[nummer].Firmabetreuer.PxName, "");
		strcpy(Eintrag[nummer].Firmabetreuer.PxVorname, "");
		strcpy(Eintrag[nummer].Firmabetreuer.PxPosition, "");
		strcpy(Eintrag[nummer].Firmabetreuer.Pxfestnetz, "");
		strcpy(Eintrag[nummer].Firmabetreuer.Pxmobil, "");
		strcpy(Eintrag[nummer].Firmabetreuer.Pxemail, "");

		Eintrag[nummer].Dateneingegeben = 0;
		//---------------------------




		//----Verschieben, falls ein Eintrag irgendwo aus der Mitte gel�scht wird
		verschieben = nummer;					//Hilfsvariable um Nummer nicht ver�ndern zu m�ssen
		while (verschieben < anzahl){			//F�llen der durchs L�schen entstandenen L�cke im Array, in dem alle nachfolgenden Eintr�ge aufger�ckt werden
			Eintrag[verschieben] = Eintrag[verschieben + 1];
			verschieben++;
		};
		//---------------------------------------------


		

		//----gleiches Verschieben f�r Suchergebnisse--
		verschiebensuch = nummer;				//Hilfsvariable um Nummer nicht ver�ndern zu m�ssen
		while (verschieben < anzahl){			//F�llen der durchs L�schen entstandenen L�cke im Array, in dem alle nachfolgenden Eintr�ge aufger�ckt werden
			ha[verschieben] = ha[verschieben + 1];
			verschieben++;
		};
		Ergebnisanzahl--;
		//---------------------------------------------




		//nummer = 0;
		zaehlen();									//Neuz�hlung der Eintr�ge

		label26->Text = Convert::ToString(anzahl);	//Ausgeben der neuen Anzahl

		MessageBox::Show("L�schen des Datensatzes erfolgreich!",
			"L�schen erfolgreich!", MessageBoxButtons::OK,
			MessageBoxIcon::Asterisk);

		//-----------AUSGABE des vorherigen Datensatzes--
		if (bearbeitenhilf == 4){
			Close();
		}

		nummer--;
		if (nummer < 0) nummer = anzahl - 1;

		textBox23->Text = Convert::ToString(nummer + 1);
		textBox1->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Studentname.Name));//NAME					umwandeln von Char array in System String
		textBox2->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Studentname.Vorname));//VORNAME				umwandeln von Char array in System String

		if (Eintrag[nummer].Studentname.Matrikelnummer != 0){								//Damit nicht "0" in den Feldern angezeigt wird, wenn nichts eingetragen wird
			textBox3->Text = Convert::ToString(Eintrag[nummer].Studentname.Matrikelnummer); //MATRIKELNUMMER
		}
		else{
			textBox3->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(""));
		};



		if (Eintrag[nummer].Studentname.Jahr != 0){								//Damit nicht "0" in den Feldern angezeigt wird, wenn nichts eingetragen wird
			textBox4->Text = Convert::ToString(Eintrag[nummer].Studentname.Jahr); //STUDIENBEGINN(Jahr)
		}
		else{
			textBox4->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(""));
		};

		textBox5->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Studentname.festnetz));//FESTNETZNUMMER		umwandeln von Char array in System String

		textBox6->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Studentname.mobil));//MOBILNUMMER			umwandeln von Char array in System String
		textBox7->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Studentname.email));//EMAIL					umwandeln von Char array in System String
		textBox8->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Studentadresse.Strasse));//STRA�E			umwandeln von Char array in System String
		textBox9->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Studentadresse.Hausnummer));//Hausnummer	umwandeln von Char array in System String

		if (Eintrag[nummer].Studentadresse.Plz != 0){								//Damit nicht "0" in den Feldern angezeigt wird, wenn nichts eingetragen wird
			textBox10->Text = Convert::ToString(Eintrag[nummer].Studentadresse.Plz); //PLZ
		}
		else{
			textBox10->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(""));
		};


		textBox11->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Studentadresse.Stadt));//STADT				umwandeln von Char array in System String
		textBox12->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Firmaname));//Px_PARTNER					umwandeln von Char array in System String
		textBox13->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Firmaadresse.PxStrasse));//STRA�E			umwandeln von Char array in System String
		textBox14->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Firmaadresse.PxHausnummer));//NUMMER		umwandeln von Char array in System String

		if (Eintrag[nummer].Firmaadresse.PxPlz != 0){								//Damit nicht "0" in den Feldern angezeigt wird, wenn nichts eingetragen wird
			textBox15->Text = Convert::ToString(Eintrag[nummer].Firmaadresse.PxPlz); //PX_ADRESSE
		}
		else{
			textBox15->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(""));
		};

		textBox16->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Firmaadresse.PxStadt));//STADT				umwandeln von Char array in System String
		textBox17->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Firmabetreuer.PxName));//BETREUER			umwandeln von Char array in System String
		textBox18->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Firmabetreuer.PxVorname));//BETREUER 	
		textBox19->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Firmabetreuer.PxPosition));//BETREUER			umwandeln von Char array in System String
		textBox20->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Firmabetreuer.Pxfestnetz));//BETREUER 	
		textBox21->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Firmabetreuer.Pxmobil));//BETREUER 	
		textBox22->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Firmabetreuer.Pxemail));//BETREUER 			//AUSGABE aller Attribute (Schreiben in die TextBoxen)	//AUSGABE aller Attribute										//AUSGABE aller Attribute

	};
		//------------Ende Ausgabe-----------------------

}


//Eventhandler �bernehmen-Button
System::Void INF3_Projekt::AnzeigenForm::button6_Click(System::Object^  sender, System::EventArgs^  e) {	//�bernehmen-button
	
	
	extern STUDENT Eintrag[150];//benutzen der externen Variablen Eintrag[100] und nummer
	extern int nummer;
	extern int anzahl;
	extern int zaehlen();
	int i = 0;					//Laufvariable f�r Schleife zur Pr�fung ob Eintrag wie gefordert eine Zahl ist
	char eingabe1[50];			//Variable zur Zwischenspeicherung bei der Pr�fung
	char eingabe2[50];
	char eingabe3[50];
	char eingabe4[50];
	bool istnichtzahl1 = false; //Ergebnis der Pr�fung
	bool istnichtzahl2 = false; //Ergebnis der Pr�fung
	bool istnichtzahl3 = false; //Ergebnis der Pr�fung
	bool istnichtzahl4 = false; //Ergebnis der Pr�fung
	bool f = true;				//Fehlervariable, falls ein Pflichtfeld nicht ausgef�llt ist

//---(Neu)einlesen aller Attribute. 1. Pr�fen ob leer. 2.(wenn nicht) Wert int Variable schreiben. 3. "Dateneingegeben"=1---------
	
	if (String::IsNullOrEmpty(textBox1->Text) && f == true)		//NAME		Abfrage, ob Pflichtfeld ausgef�llt ist
		//Pr�fen, ob Feld leer. Wenn nicht, dann den Wert der Textbox in entsprechende Variable des Studenten schreiben
	{ 
		MessageBox::Show("Bitte geben Sie den Nachnamen des Studenten ein.", "Achtung Pflichtfeld!", MessageBoxButtons::OK,
		MessageBoxIcon::Asterisk);
		//textBox1->BackColor = System::Drawing::Color::Yellow;

		f = false;
	}	 
	else if (f == true)
	{
		textBox1->BackColor = System::Drawing::Color::White;
		sprintf(Eintrag[nummer].Studentname.Name, "%s", textBox1->Text);
		Eintrag[nummer].Dateneingegeben = 1;
	}
	


	if (String::IsNullOrEmpty(textBox2->Text) && f == true)		//VORNAME	Abfrage, ob Pflichtfeld ausgef�llt ist
	{ 
		MessageBox::Show("Bitte geben Sie den Vornamen des Studenten ein.", "Achtung Pflichtfeld!", MessageBoxButtons::OK,
		MessageBoxIcon::Asterisk);
		//textBox2->BackColor = System::Drawing::Color::Yellow;
		f = false;
	}
	else if(f == true)
	{
		textBox2->BackColor = System::Drawing::Color::White;
		sprintf(Eintrag[nummer].Studentname.Vorname, "%s", textBox2->Text);				// danach Umwandeln String^ in Char Array
		Eintrag[nummer].Dateneingegeben = 1;
	}



	if (String::IsNullOrEmpty(textBox3->Text) && f == true)		//MATRIKELNUMMER
	{
		MessageBox::Show("Bitte geben Sie die Matrikelnummer des Studenten ein.", "Achtung Pflichtfeld!", MessageBoxButtons::OK,
		MessageBoxIcon::Asterisk);
		//textBox3->BackColor = System::Drawing::Color::Yellow;
		f = false;
	}
	else if (f == true)
	{
		textBox3->BackColor = System::Drawing::Color::White;
		sprintf(eingabe1, "%s", textBox3->Text);
		int laenge1 = strlen(eingabe1);							//L�nge der Eingabe errechnen

		if (laenge1 <= 6)										//keine Zahlen > 999999
		{
			i = 0;
			while (i <= laenge1 - 1)							//Pr�fen, ob alle Zeichen im eingegebenen Array Ziffern sind
			{
				if (isdigit(eingabe1[i]))
				{
				}
				else
				{
					istnichtzahl1 = true;						//Wenn nicht, dann true --> Fehlerfall
				}
				i++;
			}

			if (istnichtzahl1 == false && laenge1 <= 6)			//Abfragen der Pr�fvariablen. Wenn alles Ok, wird Attribut in Struct geschrieben
			{	
				Eintrag[nummer].Studentname.Matrikelnummer = int::Parse(textBox3->Text->ToString()); //Einlesen eines Integers aus Textbox
				Eintrag[nummer].Dateneingegeben = 1;
				textBox3->BackColor = System::Drawing::Color::White;
			}
			else
			{
				//entsprechendes Textfeld gelb f�rben
				textBox3->BackColor = System::Drawing::Color::Yellow;
			}
		}
		else //Eintr�ge > 999999 werden nicht als Zahl gewertet
		{
			istnichtzahl1 = true;
			textBox3->BackColor = System::Drawing::Color::Yellow;
		}
	}
		


	if (String::IsNullOrEmpty(textBox4->Text) && f == true)		//JAHR
	{
		MessageBox::Show("Bitte geben Sie den Studienbeginn des Studenten ein.", "Achtung Pflichtfeld!", MessageBoxButtons::OK,
		MessageBoxIcon::Asterisk);
		//textBox4->BackColor = System::Drawing::Color::Yellow;
		f = false;
	}
	else if (f == true)
	{
		textBox4->BackColor = System::Drawing::Color::White;
		sprintf(eingabe2, "%s", textBox4->Text);
		int laenge2 = strlen(eingabe2);


		if (laenge2 <= 6)
		{
		i = 0;
			while (i <= laenge2 - 1 )		//Pr�fen, ob alle Zeichen im eingegebenen Array Ziffern sind
			{
				if (isdigit(eingabe2[i]))
				{
				}
				else
				{
					istnichtzahl2 = true;	//Wenn nicht, dann true
				}
				i++;
			}
	
			if (istnichtzahl2 == false)
			{
				Eintrag[nummer].Studentname.Jahr = int::Parse(textBox4->Text->ToString());
				Eintrag[nummer].Dateneingegeben = 1;
				textBox4->BackColor = System::Drawing::Color::White;
			}
			else
			{
			//entsprechendes Textfeld gelb markieren
			textBox4->BackColor = System::Drawing::Color::Yellow;
			}
		}
		else //Eintr�ge > 999999 werden nicht als Zahl gewertet
		{
			istnichtzahl2 = true;
			textBox4->BackColor = System::Drawing::Color::Yellow;
		}
	}



	if (String::IsNullOrEmpty(textBox5->Text) && f == true)		//FESTNETZNUMMER
	{
		strcpy(Eintrag[nummer].Studentname.festnetz, ""); 
	}
	else if (f == true)
	{
		sprintf(Eintrag[nummer].Studentname.festnetz, "%s", textBox5->Text);
		Eintrag[nummer].Dateneingegeben = 1;
	}
		

	
	if (String::IsNullOrEmpty(textBox6->Text) && f == true)		//Mobilnummer
	{ 
		strcpy(Eintrag[nummer].Studentname.mobil, ""); 
	}	
	else if (f == true)
	{
		sprintf(Eintrag[nummer].Studentname.mobil, "%s", textBox6->Text);
		Eintrag[nummer].Dateneingegeben = 1;
	}
	
	if (String::IsNullOrEmpty(textBox7->Text) && f == true)		//EMAIL
	{ 
		strcpy(Eintrag[nummer].Studentname.email, ""); 
	}
	else if (f == true)
	{
		sprintf(Eintrag[nummer].Studentname.email, "%s", textBox7->Text);
		Eintrag[nummer].Dateneingegeben = 1;
	}

	if (String::IsNullOrEmpty(textBox8->Text) && f == true)		//STRASSE
	{ 
		strcpy(Eintrag[nummer].Studentadresse.Strasse, ""); 
	}
	else if(f == true)
	{
		sprintf(Eintrag[nummer].Studentadresse.Strasse, "%s", textBox8->Text);
		Eintrag[nummer].Dateneingegeben = 1;
	}
		
	if (String::IsNullOrEmpty(textBox9->Text) && f == true)		//Hausnummer
	{ 
		strcpy(Eintrag[nummer].Studentadresse.Hausnummer, ""); 
	}
	else if (f == true)
	{
		sprintf(Eintrag[nummer].Studentadresse.Hausnummer, "%s", textBox9->Text);
		Eintrag[nummer].Dateneingegeben = 1;
	}

	if (String::IsNullOrEmpty(textBox10->Text) && f == true)	//PLZ
	{
		Eintrag[nummer].Studentadresse.Plz = 0; 
	}
	else if (f == true)
	{
		sprintf(eingabe3, "%s", textBox10->Text);
		int laenge3 = strlen(eingabe3);

		
		
		if (laenge3 <= 6)
		{
			i = 0;
			while (i <= laenge3 - 1)		//Pr�fen, ob alle Zeichen im eingegebenen Array Ziffern sind
			{
				if (isdigit(eingabe3[i]))
				{
				}
				else
				{
					istnichtzahl3 = true;	//Wenn nicht, dann true
				}
				i++;
			}

			if (istnichtzahl3 == false)
			{
				Eintrag[nummer].Studentadresse.Plz = int::Parse(textBox10->Text->ToString());
				Eintrag[nummer].Dateneingegeben = 1;
				textBox10->BackColor = System::Drawing::Color::White;
			}
			else
			{
				//entsprechendes Textfeld rot umrahmen
				textBox10->BackColor = System::Drawing::Color::Yellow;
			}
		}
		else //Eintr�ge > 999999 werden nicht als Zahl gewertet
		{
		istnichtzahl3 = true;
		textBox10->BackColor = System::Drawing::Color::Yellow;
		}
	}

	if (String::IsNullOrEmpty(textBox11->Text) && f == true)	//STADT
	{
		strcpy(Eintrag[nummer].Studentadresse.Stadt, ""); 
	}
	else if (f == true)
	{
		sprintf(Eintrag[nummer].Studentadresse.Stadt, "%s", textBox11->Text);
		Eintrag[nummer].Dateneingegeben = 1;
	}
		
	if (String::IsNullOrEmpty(textBox12->Text) && f == true)	//Praxispartner
	{
		MessageBox::Show("Bitte geben Sie den Namen des Praxispartners des Studenten ein.", "Achtung Pflichtfeld!", MessageBoxButtons::OK,
		MessageBoxIcon::Asterisk);
		//textBox12->BackColor = System::Drawing::Color::Yellow;
		f = false;
	}
	else if (f == true)
	{
		sprintf(Eintrag[nummer].Firmaname, "%s", textBox12->Text);
		Eintrag[nummer].Dateneingegeben = 1;
		textBox12->BackColor = System::Drawing::Color::White;
	}

	if (String::IsNullOrEmpty(textBox13->Text) && f == true)	//PXSTRASSE
	{
		strcpy(Eintrag[nummer].Firmaadresse.PxStrasse, ""); 
	}
	else if (f == true)
	{
		sprintf(Eintrag[nummer].Firmaadresse.PxStrasse, "%s", textBox13->Text);
		Eintrag[nummer].Dateneingegeben = 1;
	}
	
	if (String::IsNullOrEmpty(textBox14->Text) && f == true)	//Hausnummer
	{
		strcpy(Eintrag[nummer].Firmaadresse.PxHausnummer, ""); 
	}
	else if (f == true)
	{
		sprintf(Eintrag[nummer].Firmaadresse.PxHausnummer, "%s", textBox14->Text);
		Eintrag[nummer].Dateneingegeben = 1;
	}

	if (String::IsNullOrEmpty(textBox15->Text) && f == true)	//PXPLZ
	{
		Eintrag[nummer].Firmaadresse.PxPlz = 0; 
	}
	else if (f == true)
	{
		sprintf(eingabe4, "%s", textBox15->Text);
		int laenge4 = strlen(eingabe4);


		if (laenge4 <= 6)
		{
			i = 0;
			while (i <= laenge4 - 1)		//Pr�fen, ob alle Zeichen im eingegebenen Array Ziffern sind
			{
				if (isdigit(eingabe4[i]))
				{
				}
				else
				{
					istnichtzahl4 = true;	//Wenn nicht, dann true
				}
				i++;
			}

			if (istnichtzahl4 == false)
			{
				Eintrag[nummer].Firmaadresse.PxPlz = int::Parse(textBox15->Text->ToString());
				Eintrag[nummer].Dateneingegeben = 1;
				textBox15->BackColor = System::Drawing::Color::White;
			}
			else
			{
				//entsprechendes Textfeld rot umrahmen
				textBox15->BackColor = System::Drawing::Color::Yellow;
			}
		}
		else //Eintr�ge > 999999 werden nicht als Zahl gewertet
		{
			istnichtzahl4 = true;
			textBox15->BackColor = System::Drawing::Color::Yellow;
		}
	}

	if (String::IsNullOrEmpty(textBox16->Text) && f == true){ strcpy(Eintrag[nummer].Firmaadresse.PxStadt, ""); }	//PxSTADT
	else if (f == true)
	{
		sprintf(Eintrag[nummer].Firmaadresse.PxStadt, "%s", textBox16->Text);
		Eintrag[nummer].Dateneingegeben = 1;
	}

	if (String::IsNullOrEmpty(textBox17->Text) && f == true){ strcpy(Eintrag[nummer].Firmabetreuer.PxName, ""); }	//PxName Betreuer
	else if (f == true)
	{
		sprintf(Eintrag[nummer].Firmabetreuer.PxName, "%s", textBox17->Text);
		Eintrag[nummer].Dateneingegeben = 1;
	}
	
	if (String::IsNullOrEmpty(textBox18->Text) && f == true){ strcpy(Eintrag[nummer].Firmabetreuer.PxVorname, ""); }	//Vorname Betreuer
	else if (f == true)
	{
		sprintf(Eintrag[nummer].Firmabetreuer.PxVorname, "%s", textBox18->Text);
		Eintrag[nummer].Dateneingegeben = 1;
	}
	
	if (String::IsNullOrEmpty(textBox19->Text) && f == true){ strcpy(Eintrag[nummer].Firmabetreuer.PxPosition, ""); }	//Position
	else if (f == true)
	{
		sprintf(Eintrag[nummer].Firmabetreuer.PxPosition, "%s", textBox19->Text);
		Eintrag[nummer].Dateneingegeben = 1;
	}

	if (String::IsNullOrEmpty(textBox20->Text) && f == true){ strcpy(Eintrag[nummer].Firmabetreuer.Pxfestnetz, ""); }	//Nummer Festnetz Betreuer
	else if (f == true)
	{
		sprintf(Eintrag[nummer].Firmabetreuer.Pxfestnetz, "%s", textBox20->Text);
		Eintrag[nummer].Dateneingegeben = 1;
	}
	
	if (String::IsNullOrEmpty(textBox21->Text) && f == true){ strcpy(Eintrag[nummer].Firmabetreuer.Pxmobil, ""); }		//Nummer Mobil Betreuer
	else if (f == true)
	{
		sprintf(Eintrag[nummer].Firmabetreuer.Pxmobil, "%s", textBox21->Text);
		Eintrag[nummer].Dateneingegeben = 1;
	}
		
	if (String::IsNullOrEmpty(textBox22->Text) && f == true){ strcpy(Eintrag[nummer].Firmabetreuer.Pxemail, ""); }		//PxEMAIL
	else if (f == true)
	{
		sprintf(Eintrag[nummer].Firmabetreuer.Pxemail, "%s", textBox22->Text);
		Eintrag[nummer].Dateneingegeben = 1;
	}			
	
	
//--------------------------------------------------------------------------------------------------------------------------------
		
		button6->Enabled = false;					//nachdem einmal �bernommen wurde, kann nicht nochmal �bernommen werden (erst wieder, wenn eine Textbox ge�ndert wurde)
		zaehlen();									//Z�hlen der vorhandenen Eintr�ge
		
		label26->Text = Convert::ToString(anzahl);	//Anzahl Eintr�ge ausgeben
	
	
		if (istnichtzahl1 == true || istnichtzahl2 == true || istnichtzahl3 == true || istnichtzahl4 == true) //Wenn ein Zahlenattribut nicht als reine Zahl eingegeben wurde
		{
			MessageBox::Show("Falsche Eingabe!\n\nBitte geben Sie im gelb markierten Feld eine Nummer zwischen 0 und 999999 ein.", "Falsche Eingabe!",
				MessageBoxButtons::OK,
				MessageBoxIcon::Asterisk);
		}
		else //Wenn alle Eintr�ge gem�� Ordnung
		{
			
			if (bearbeitenhilf == 3 && f ==true)			//Wenn Anzeigefenster als �ndern
			{
				Close();									//Schlie�en des Fensters, da nur dieser Eintrag bearbeitet werden soll
			}
			else if (bearbeitenhilf == 1 && f == true)		// Wenn Anzeigenfenster als Eingabefenster ge�ffnet
			{
				zaehlen();

				if (anzahl <= 149)							//Pr�fen ob Datenbank voll. Wenn nicht (anzahl<=149), dann k�nnen weitere vorgenommen werden
				{

					if (MessageBox::Show(					//Abfragen, ob weiterer Datensatz eingegeben werden soll
						"Datensatz erfolgreich eingetragen! Weiteren Datensatz eingeben?",
						"N�chster Datensatz?", MessageBoxButtons::YesNo,
						MessageBoxIcon::Question) == ::System::Windows::Forms::DialogResult::Yes)
					{

						nummer++; //Wenn ja, dann Nummer erh�hen

						//-------aktualisieren der Textboxen f�r n�chste Nummer---------------------
						textBox23->Text = Convert::ToString(nummer + 1);
						textBox1->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Studentname.Name));//NAME					umwandeln von Char array in System String
						textBox2->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Studentname.Vorname));//VORNAME				umwandeln von Char array in System String

						if (Eintrag[nummer].Studentname.Matrikelnummer != 0){								//Damit nicht "0" in den Feldern angezeigt wird, wenn nichts eingetragen wird
							textBox3->Text = Convert::ToString(Eintrag[nummer].Studentname.Matrikelnummer); //MATRIKELNUMMER
						}
						else{
							textBox3->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(""));
						};



						if (Eintrag[nummer].Studentname.Jahr != 0){								//Damit nicht "0" in den Feldern angezeigt wird, wenn nichts eingetragen wird
							textBox4->Text = Convert::ToString(Eintrag[nummer].Studentname.Jahr); //STUDIENBEGINN(Jahr)
						}
						else{
							textBox4->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(""));
						};

						textBox5->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Studentname.festnetz));//FESTNETZNUMMER		umwandeln von Char array in System String

						textBox6->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Studentname.mobil));//MOBILNUMMER			umwandeln von Char array in System String
						textBox7->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Studentname.email));//EMAIL					umwandeln von Char array in System String
						textBox8->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Studentadresse.Strasse));//STRA�E			umwandeln von Char array in System String
						textBox9->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Studentadresse.Hausnummer));//Hausnummer	umwandeln von Char array in System String

						if (Eintrag[nummer].Studentadresse.Plz != 0){								//Damit nicht "0" in den Feldern angezeigt wird, wenn nichts eingetragen wird
							textBox10->Text = Convert::ToString(Eintrag[nummer].Studentadresse.Plz); //PLZ
						}
						else{
							textBox10->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(""));
						};


						textBox11->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Studentadresse.Stadt));//STADT				umwandeln von Char array in System String
						textBox12->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Firmaname));//Px_PARTNER					umwandeln von Char array in System String
						textBox13->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Firmaadresse.PxStrasse));//STRA�E			umwandeln von Char array in System String
						textBox14->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Firmaadresse.PxHausnummer));//NUMMER		umwandeln von Char array in System String

						if (Eintrag[nummer].Firmaadresse.PxPlz != 0){								//Damit nicht "0" in den Feldern angezeigt wird, wenn nichts eingetragen wird
							textBox15->Text = Convert::ToString(Eintrag[nummer].Firmaadresse.PxPlz); //PX_ADRESSE
						}
						else{
							textBox15->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(""));
						};

						textBox16->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Firmaadresse.PxStadt));//STADT				umwandeln von Char array in System String
						textBox17->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Firmabetreuer.PxName));//BETREUER			umwandeln von Char array in System String
						textBox18->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Firmabetreuer.PxVorname));//BETREUER 	
						textBox19->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Firmabetreuer.PxPosition));//BETREUER			umwandeln von Char array in System String
						textBox20->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Firmabetreuer.Pxfestnetz));//BETREUER 	
						textBox21->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Firmabetreuer.Pxmobil));//BETREUER 	
						textBox22->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Firmabetreuer.Pxemail));//BETREUER 			//AUSGABE aller Attribute (Schreiben in die TextBoxen)	//AUSGABE aller Attribute										//AUSGABE aller Attribute
						
						//--------------------------------------------------------------------------

						button6->Enabled = false;			//nachdem einmal �bernommen wurde, kann nicht nochmal �bernommen werden (erst wieder, wenn eine Textbox ge�ndert wurde)
					}
					else
					{
						Close();
					}

				}
				else
				{
					MessageBox::Show("Datensatz eingetragen. Es wurde die maximale Anzahl an Eintr�gen vorgenommen.", "Datenbank voll",
						MessageBoxButtons::OK,
						MessageBoxIcon::Asterisk);
					Close();
				}
			}

		}
		
		
}


//Eventhandler "Wenn Textbox23 (Eintragsnummer, oben links) ge�ndert". WAHRSCHEINLICH L�SCHEN oder reparieren! 
System::Void INF3_Projekt::AnzeigenForm::textBox23_TextChanged(System::Object^  sender, System::EventArgs^  e)
{	
	//NAVIGIEREN zu bestimmter Nummer �ber TextBox

	extern int nummer;
	extern STUDENT Eintrag[150];
	
	nummer=int::Parse(textBox23->Text->ToString())-1;		//einlesen aus einer Textbox als Integer (Nummerabfrage)
	if (nummer < 0) nummer = 0;
	if (nummer >= 150) nummer = 149;
	
	textBox23->Text = Convert::ToString(nummer + 1);
	textBox1->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Studentname.Name));//NAME					umwandeln von Char array in System String
	textBox2->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Studentname.Vorname));//VORNAME				umwandeln von Char array in System String

	if (Eintrag[nummer].Studentname.Matrikelnummer != 0){								//Damit nicht "0" in den Feldern angezeigt wird, wenn nichts eingetragen wird
		textBox3->Text = Convert::ToString(Eintrag[nummer].Studentname.Matrikelnummer); //MATRIKELNUMMER
	}
	else{
		textBox3->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(""));
	};



	if (Eintrag[nummer].Studentname.Jahr != 0){								//Damit nicht "0" in den Feldern angezeigt wird, wenn nichts eingetragen wird
		textBox4->Text = Convert::ToString(Eintrag[nummer].Studentname.Jahr); //STUDIENBEGINN(Jahr)
	}
	else{
		textBox4->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(""));
	};

	textBox5->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Studentname.festnetz));//FESTNETZNUMMER		umwandeln von Char array in System String

	textBox6->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Studentname.mobil));//MOBILNUMMER			umwandeln von Char array in System String
	textBox7->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Studentname.email));//EMAIL					umwandeln von Char array in System String
	textBox8->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Studentadresse.Strasse));//STRA�E			umwandeln von Char array in System String
	textBox9->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Studentadresse.Hausnummer));//Hausnummer	umwandeln von Char array in System String

	if (Eintrag[nummer].Studentadresse.Plz != 0){								//Damit nicht "0" in den Feldern angezeigt wird, wenn nichts eingetragen wird
		textBox10->Text = Convert::ToString(Eintrag[nummer].Studentadresse.Plz); //PLZ
	}
	else{
		textBox10->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(""));
	};


	textBox11->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Studentadresse.Stadt));//STADT				umwandeln von Char array in System String
	textBox12->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Firmaname));//Px_PARTNER					umwandeln von Char array in System String
	textBox13->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Firmaadresse.PxStrasse));//STRA�E			umwandeln von Char array in System String
	textBox14->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Firmaadresse.PxHausnummer));//NUMMER		umwandeln von Char array in System String

	if (Eintrag[nummer].Firmaadresse.PxPlz != 0){								//Damit nicht "0" in den Feldern angezeigt wird, wenn nichts eingetragen wird
		textBox15->Text = Convert::ToString(Eintrag[nummer].Firmaadresse.PxPlz); //PX_ADRESSE
	}
	else{
		textBox15->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(""));
	};

	textBox16->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Firmaadresse.PxStadt));//STADT				umwandeln von Char array in System String
	textBox17->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Firmabetreuer.PxName));//BETREUER			umwandeln von Char array in System String
	textBox18->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Firmabetreuer.PxVorname));//BETREUER 	
	textBox19->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Firmabetreuer.PxPosition));//BETREUER			umwandeln von Char array in System String
	textBox20->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Firmabetreuer.Pxfestnetz));//BETREUER 	
	textBox21->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Firmabetreuer.Pxmobil));//BETREUER 	
	textBox22->Text = System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(Eintrag[nummer].Firmabetreuer.Pxemail));//BETREUER 			//AUSGABE aller Attribute (Schreiben in die TextBoxen)		//AUSGABE aller Attribute										//AUSGABE aller Attribute

	




}
