#include <stdio.h>
#include <conio.h>
#include <string>

#define Max 50					//MAX-Variable, damit alle String-L�ngen leicht zusammen ver�nderbar sind

//int Eintrag[150];

typedef struct{					//Datentyp f�r Stammdaten des Studenten (Name, Mat.Nr, Immatrikulationsjahr)
	char Name[Max];
	char Vorname[Max];
	int Matrikelnummer;
	int Jahr;
	char festnetz[Max];
	char mobil[Max];			//String-Variablen, damit Leerzeichen zwischen Vorwahl und Nummer m�glich sind
	char email[Max];
} NAME;

typedef struct{					//Datentyp f�r die Studentenadresse
	char Strasse[Max];			//L�nge gemessen an l�ngsten Stra�ennamen Deutschlands
	char Hausnummer[Max];		//Char damit "8a"(bsp) m�glich ist
	int Plz;
	char Stadt[Max];
} ADRESSE;

//typedef struct{
//Mobilnummer
//Festnetz
//} TELEFON;

//EMAILADRESSE

typedef struct{					//Datentyp f�r Firmenadresse
	char PxStrasse[Max];
	char PxHausnummer[Max];
	int PxPlz;
	char PxStadt[Max];
} PXADRESSE;

typedef struct{					//Datentyp f�r Ansprechpartner in der Firma
	char PxName[Max];
	char PxVorname[Max];
	char PxPosition[Max];
	char Pxfestnetz[Max];
	char Pxmobil[Max];
	char Pxemail[Max];

} ANSPRECHPARTNER;

typedef struct{					//HAUPTSTRUCT f�r Studenten

	NAME Studentname;
	ADRESSE Studentadresse;
	//TELEFON Studenttelefon;
	//EMAILADRESSE;
	char Firmaname[Max];		//String f�r Name des Praxispartners
	PXADRESSE Firmaadresse;
	ANSPRECHPARTNER Firmabetreuer;
	int Dateneingegeben;
}STUDENT;










